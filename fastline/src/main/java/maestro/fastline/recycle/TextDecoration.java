package maestro.fastline.recycle;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by maestro123 on 02.06.2016.
 */
public class TextDecoration extends SpaceItemDecoration {

    private DecorationTextHeader mHeader;
    private DecorationTextHeader.DecorationHeaderDrawCallback mCallback;

    private int mHeaderStartOffset = 0;

    public TextDecoration(Context context, int headerStyle) {
        super(context);
        mHeader = new DecorationTextHeader(context, headerStyle);
    }

    public TextDecoration(Context context, int resId, int headerStyle) {
        super(context, resId);
        mHeader = new DecorationTextHeader(context, headerStyle);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int adapterPosition = parent.getChildAdapterPosition(view);
        if (shouldDrawHeader(parent.getAdapter(), adapterPosition)) {
            outRect.top += mHeader.getHeight();
            if (adapterPosition == 0) {
                outRect.top += mHeaderStartOffset;
            }
        }
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        RecyclerView.Adapter adapter = parent.getAdapter();
        if (adapter == null) {
            return;
        }

        int offset = 0;
        if (getSpaceSize() > 0) {
            offset = getSpaceSize() >> 1;
        }

        for (int i = 0; i < parent.getChildCount(); i++) {
            View v = parent.getChildAt(i);
            if (v == null) {
                continue;
            }
            RecyclerView.ViewHolder holder = parent.getChildViewHolder(v);
            int adapterPosition = holder.getAdapterPosition();

            if (adapterPosition == -1) {
                continue;
            }

            if (shouldDrawHeader(adapter, adapterPosition)) {
                Rect bounds = new Rect(parent.getPaddingLeft(), (int) (v.getY() - mHeader.getHeight()) - offset, parent.getWidth() - parent.getPaddingRight(), (int) (v.getY() - offset));
                mHeader.draw(c, bounds, ((DecorationHeaderAdapter) adapter).getHeaderText(adapterPosition), adapterPosition, mCallback);
            }

            onDrawOver(c, adapter, holder, v);
        }
    }

    public void onDrawOver(Canvas canvas, RecyclerView.Adapter adapter, RecyclerView.ViewHolder holder, View v){}

    @Override
    protected boolean canDraw(View view, RecyclerView recyclerView) {
        return super.canDraw(view, recyclerView) && isNextHaveHeader(recyclerView.getAdapter(), recyclerView.getChildAdapterPosition(view));
    }

    public TextDecoration setHeaderStartOffset(int offset) {
        mHeaderStartOffset = offset;
        return this;
    }

    public TextDecoration setHeaderCallback(DecorationTextHeader.DecorationHeaderDrawCallback callback) {
        mCallback = callback;
        return this;
    }

    protected boolean shouldDrawHeader(RecyclerView.Adapter adapter, int adapterPosition) {
        if (adapter instanceof DecorationHeaderAdapter) {
            DecorationHeaderAdapter headerAdapter = (DecorationHeaderAdapter) adapter;
            return headerAdapter.haveHeader(adapterPosition);
        }
        return false;
    }

    protected boolean isNextHaveHeader(RecyclerView.Adapter adapter, int adapterPosition) {
        return !(adapterPosition == -1 || adapterPosition + 1 >= adapter.getItemCount()) && shouldDrawHeader(adapter, adapterPosition + 1);
    }

    public interface DecorationHeaderAdapter {
        CharSequence getHeaderText(int position);

        boolean haveHeader(int position);
    }

}
