package maestro.fastline.recycle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by maestro123 on 25.05.2016.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    public static final String TAG = DividerItemDecoration.class.getSimpleName();

    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    private Drawable mDivider;
    private Rect mMargins = new Rect();

    public DividerItemDecoration(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        mDivider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    public DividerItemDecoration(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }

    public DividerItemDecoration setMargin(int left, int top, int right, int bottom) {
        mMargins.set(left, top, right, bottom);
        return this;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft() + mMargins.left;
        int right = parent.getWidth() - parent.getPaddingRight() - mMargins.right;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            if (parent.getChildAdapterPosition(child) == parent.getAdapter().getItemCount() - 1 && !drawLast()) {
                continue;
            }
            if (!canDraw(child, parent)) {
                continue;
            }

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin + mMargins.top;
            int bottom = top + mDivider.getIntrinsicHeight() - mMargins.bottom;

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    protected boolean drawLast() {
        return false;
    }

    protected boolean canDraw(View view, RecyclerView recyclerView) {
        return true;
    }

}
