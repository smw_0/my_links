package maestro.fastline.recycle;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom on 12.02.2016.
 */
public abstract class BaseRecyclerAdapter<DataType, VH extends BaseViewHolder>
        extends RecyclerView.Adapter<VH> implements ModernRecyclerView.IStateAdapter,
        View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = BaseRecyclerAdapter.class.getSimpleName();

    public static final int TYPE_HEADER = -2;
    public static final int TYPE_FOOTER = -3;

    private Context mContext;
    private LayoutInflater mInflater;

    private ArrayList<DataType> mItems = new ArrayList<DataType>();

    private ArrayList<View> mHeaderViews = new ArrayList<View>();
    private ArrayList<View> mFooterViews = new ArrayList<View>();

    private OnItemClickListener mItemClickListener;
    private OnItemLongClickListener mItemLongClickListener;
    private OnAdditionalClickListener mHeaderClickListener;
    private OnAdditionalClickListener mFooterClickListener;

    private int mHeaderCount;
    private int mFooterCount;

    private boolean isAttached = false;

    private boolean haveHeader = false;
    private boolean haveFooter = false;

    private boolean isHeaderAlwaysVisible = false;
    private boolean isFooterAlwaysVisible = false;

    private boolean isItemClickable = true;
    private boolean isItemLongClickable = true;

    public BaseRecyclerAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setHasStableIds(true);
    }

    @Override
    public int getItemCount() {
        int originCount = getOriginCount();
        int count = 0;
        if (haveHeader && (originCount > 0 || isHeaderAlwaysVisible)) {
            count += getHeaderCount();
        }
        if (haveFooter && (originCount > 0 || isFooterAlwaysVisible)) {
            count += getFooterCount();
        }
        return count + originCount;
    }

    @Override
    public long getItemId(int position) {
        if (isHeaderPosition(position) || isFooterPosition(position)) {
            return Long.MIN_VALUE + position;
        } else {
            return getOriginItemId(position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeaderPosition(position)) {
            return getHeaderTypeId(position);
        } else if (isFooterPosition(position)) {
            return getFooterTypeId(position);
        }
        return getOriginItemViewType(getItemPosition(position));
    }

    @Override
    public VH onCreateViewHolder(ViewGroup viewGroup, int type) {
        BaseViewHolder holder;
        if (isHeaderType(type)) {
            holder = createHeader(viewGroup, type);
            holder.ItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invokeHeaderClick(v, (VH) v.getTag());
                }
            });
        } else if (isFooterType(type)) {
            holder = createFooter(viewGroup, type);
            holder.ItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invokeFooterClick(v, (VH) v.getTag());
                }
            });
        } else {
            holder = createItemHolder(viewGroup, type);
        }
        holder.onCreate();
        return (VH) holder;
    }

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        final int type = getItemViewType(position);
        viewHolder.setId(getItemId(position));
        viewHolder.setItemPosition(getItemPosition(position));
        if (isHeaderType(type)) {
            bindHeader(viewHolder, position, position);
            return;
        } else if (isFooterType(type)) {
            bindFooter(viewHolder, position, position - getHeaderCount() - getOriginCount());
            return;
        }
        DataType data = getItem(position);
        viewHolder.forceSetData(data);
        if (isItemClickable(data, position)) {
            viewHolder.itemView.setEnabled(true);
            viewHolder.itemView.setOnClickListener(this);
            viewHolder.itemView.setOnLongClickListener(this);
        } else {
            viewHolder.itemView.setEnabled(false);
            viewHolder.itemView.setOnClickListener(null);
            viewHolder.itemView.setOnLongClickListener(null);
        }
        bindItemHolder(viewHolder, getItem(position), position, viewHolder.getItemPosition());
    }

    @Override
    public void onClick(View v) {
        invokeItemClick(v, (VH) v.getTag());
    }

    @Override
    public boolean onLongClick(View v) {
        return invokeItemLongClick(v, (VH) v.getTag());
    }

    protected BaseViewHolder createHeader(ViewGroup group, int type) {
        return BasePlaceViewHolder.create(getContext());
    }

    protected void bindHeader(BaseViewHolder holder, int position, int headerPosition) {
        if (holder instanceof BasePlaceViewHolder) {
            BasePlaceViewHolder placeHolder = (BasePlaceViewHolder) holder;
            placeHolder.bindView(mHeaderViews.get(headerPosition));
        }
    }

    protected BaseViewHolder createFooter(ViewGroup group, int type) {
        return BasePlaceViewHolder.create(getContext());
    }

    protected void bindFooter(BaseViewHolder holder, int position, int footerPosition) {
        if (holder instanceof BasePlaceViewHolder) {
            BasePlaceViewHolder placeHolder = (BasePlaceViewHolder) holder;
            placeHolder.bindView(mFooterViews.get(footerPosition));
        }
    }

    protected abstract VH createItemHolder(ViewGroup group, int type);

    protected abstract void bindItemHolder(VH holder, DataType dataType, int position, int itemPosition);

    public void onUpdate() {
    }

    @Override
    public void onAttach() {
        isAttached = true;
    }

    @Override
    public void onDetach() {
        isAttached = false;
    }

    public int getOriginItemViewType(int position) {
        return 0;
    }

    public long getOriginItemId(int position) {
        return position;
    }

    public boolean isAttached() {
        return isAttached;
    }

    public DataType getItem(int position) {
        return getItems().get(getItemPosition(position));
    }

    public DataType getItemAtPosition(int position) {
        return getItems().get(position);
    }

    public DataType getItemById(int id) {
        return getItems().get(id);
    }

    public int getItemPosition(int position) {
        return position - getHeaderCount();
    }

    public Context getContext() {
        return mContext;
    }

    public LayoutInflater getInflater() {
        return mInflater;
    }

    public void update(List<DataType> items) {
        mItems.clear();
        if (items != null) {
            mItems.addAll(items);
        }
        onUpdate();
        notifyDataSetChanged();
    }

    public void updateNotForce(List<DataType> items) {
        mItems.clear();
        if (items != null) {
            mItems.addAll(items);
        }
        onUpdate();
    }

    public void add(DataType data) {
        getItems().add(data);
    }

    public boolean remove(DataType data) {
        if (getItems().remove(data)) {
            onItemRemoved(data);
            return true;
        }
        return false;
    }

    public boolean remove(int id) {
        DataType removedItem = getItems().remove(id);
        if (removedItem != null) {
            onItemRemoved(removedItem);
        }
        return removedItem != null;
    }

    public void onItemRemoved(DataType data) {
    }

    public void clear() {
        getItems().clear();
        onClear();
        notifyDataSetChanged();
    }

    public void onClear() {
    }

    public ArrayList<DataType> getItems() {
        return mItems;
    }

    public int getOriginCount() {
        return getItems() != null ? getItems().size() : 0;
    }

    public View inflate(ViewGroup parent, int layoutResource) {
        return mInflater.inflate(layoutResource, parent, false);
    }

    public void addHeader(View view) {
        mHeaderViews.add(view);
        ensureHaveHeader();
    }

    public void addHeader(View view, int position) {
        mHeaderViews.add(position, view);
        ensureHaveHeader();
    }

    public void removeHeader(View view) {
        mHeaderViews.remove(view);
        ensureHaveHeader();
    }

    public void addFooter(View v) {
        mFooterViews.add(v);
        ensureHaveFooter();
    }

    public void removeFooter(View v) {
        mFooterViews.remove(v);
        ensureHaveFooter();
    }

    public void setHaveHeader(boolean have) {
        setHeaderCount(have ? 1 : 0);
    }

    public void setHaveFooter(boolean have) {
        setFooterCount(have ? 1 : 0);
    }

    public void setHeaderCount(int count) {
        mHeaderCount = count;
        ensureHaveHeader();
    }

    public void setFooterCount(int count) {
        mFooterCount = count;
        ensureHaveFooter();
    }

    private void ensureHaveHeader() {
        haveHeader = mHeaderCount > 0 || mHeaderViews.size() > 0;
    }

    private void ensureHaveFooter() {
        haveFooter = mFooterCount > 0 || mFooterViews.size() > 0;
    }

    public void setHeaderAlwaysVisible(boolean visible) {
        isHeaderAlwaysVisible = visible;
    }

    public void setFooterAlwaysVisible(boolean visible) {
        isFooterAlwaysVisible = visible;
    }

    public int getHeaderCount() {
        return mHeaderCount + mHeaderViews.size();
    }

    public int getFooterCount() {
        return mFooterCount + mFooterViews.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mItemLongClickListener = listener;
    }

    public void setOnHeaderClickListener(OnAdditionalClickListener listener) {
        mHeaderClickListener = listener;
    }

    public void setOnFooterClickListener(OnAdditionalClickListener listener) {
        mFooterClickListener = listener;
    }

    public void setItemClickable(boolean clickable) {
        isItemClickable = clickable;
    }

    public void setItemLongClickable(boolean clickable) {
        isItemLongClickable = clickable;
    }

    public boolean isItemClickable(DataType dataType, int position) {
        return true;
    }

    private boolean isHeaderPosition(int position) {
        return haveHeader && position < getHeaderCount();
    }

    private boolean isFooterPosition(int position) {
        return haveFooter && position >= getOriginCount() + getHeaderCount();
    }

    protected int getHeaderTypeId(int position) {
        return TYPE_HEADER;
    }

    protected int getFooterTypeId(int position) {
        return TYPE_FOOTER;
    }

    protected boolean isHeaderType(int type) {
        return type == TYPE_HEADER;
    }

    protected boolean isFooterType(int type) {
        return type == TYPE_FOOTER;
    }

    protected void invokeItemClick(View v, VH holder) {
        if (isItemClickable && mItemClickListener != null) {
            mItemClickListener.onItemClick(v, holder.getAttachedData(), holder.getAdapterPosition());
        }
    }

    protected boolean invokeItemLongClick(View v, VH holder) {
        return isItemLongClickable
                && mItemLongClickListener != null
                && mItemLongClickListener.onItemLongClick(v, holder.getAttachedData(), holder.getAdapterPosition());
    }

    protected void invokeHeaderClick(View v, VH holder) {
        if (mHeaderClickListener != null) {
            mHeaderClickListener.onClick(v, holder.getAdapterPosition());
        }
    }

    protected void invokeFooterClick(View v, VH holder) {
        if (mFooterClickListener != null) {
            mFooterClickListener.onClick(v, holder.getAdapterPosition() - getOriginCount() - getHeaderCount());
        }
    }

    public interface OnItemClickListener<DataType> {
        void onItemClick(View v, DataType dataType, int position);
    }

    public interface OnItemLongClickListener<DataType> {
        boolean onItemLongClick(View v, DataType dataType, int position);
    }

    public interface OnAdditionalClickListener {
        void onClick(View v, int position);
    }

}
