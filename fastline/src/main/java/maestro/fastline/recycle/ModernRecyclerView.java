package maestro.fastline.recycle;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by Artyom on 25.02.2016.
 */
public class ModernRecyclerView extends RecyclerView {

    public static final String TAG = ModernRecyclerView.class.getSimpleName();

    private View mEmptyView;
    private Adapter mAdapter;
    private ChangeObserver mObserver;

    private MotionEvent mLastMotion;

    private boolean isTrackingTouch = false;

    private ArrayList<ItemDecoration> mDecorations = new ArrayList<>();

    public ModernRecyclerView(Context context) {
        super(context);
    }

    public ModernRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ModernRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ensureDetach(mAdapter);
        if (mLastMotion != null) {
            mLastMotion.recycle();
        }
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        ensureDetach(mAdapter);
        if (mAdapter != null && mObserver != null) {
            mAdapter.unregisterAdapterDataObserver(mObserver);
        }
        mAdapter = adapter;
        if (mAdapter != null) {
            if (mObserver == null) {
                mObserver = new ChangeObserver();
            }
            mAdapter.registerAdapterDataObserver(mObserver);
            if (mAdapter instanceof ISizeReceiveAdapter
                    && getMeasuredWidth() > 0
                    && getMeasuredHeight() > 0) {
                ((ISizeReceiveAdapter) mAdapter).onSizeChange(getMeasuredWidth(), getMeasuredHeight());
            }
        }
        ensureAttach(adapter);
        ensureAdapterChange();
        ensureEmptyView();
    }

    @Override
    public void addItemDecoration(ItemDecoration decor, int index) {
        super.addItemDecoration(decor, index);
        mDecorations.add(decor);
    }

    @Override
    public void removeItemDecoration(ItemDecoration decor) {
        super.removeItemDecoration(decor);
        mDecorations.remove(decor);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mAdapter instanceof ISizeReceiveAdapter && w > 0 && h > 0) {
            ((ISizeReceiveAdapter) mAdapter).onSizeChange(w, h);
        }
    }

    public void setTrackingTouch(boolean trackingTouch) {
        isTrackingTouch = trackingTouch;
        if (!isTrackingTouch && mLastMotion != null) {
            mLastMotion.recycle();
        }
    }

    public boolean hasDecoration(Class cls) {
        for (ItemDecoration decoration : mDecorations) {
            if (cls.isInstance(decoration)) {
                return true;
            }
        }
        return false;
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                && mEmptyView != null
                && mEmptyView.getImportantForAccessibility() == IMPORTANT_FOR_ACCESSIBILITY_AUTO) {
            mEmptyView.setImportantForAccessibility(IMPORTANT_FOR_ACCESSIBILITY_YES);
        }
        ensureEmptyView();
    }

    public MotionEvent getLastMotion() {
        return mLastMotion;
    }

    private void ensureEmptyView() {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(mAdapter == null || mAdapter.getItemCount() == 0
                    ? View.VISIBLE : View.GONE);
        }
    }

    private void ensureAttach(Adapter adapter) {
        if (adapter instanceof IStateAdapter) {
            ((IStateAdapter) adapter).onAttach();
        }
    }

    private void ensureDetach(Adapter adapter) {
        if (adapter instanceof IStateAdapter) {
            ((IStateAdapter) adapter).onDetach();
        }
    }

    private void ensureAdapterChange() {
        if (mAdapter != null) {
            for (ItemDecoration decoration : mDecorations) {
                if (decoration instanceof OnAdapterChangeListener) {
                    ((OnAdapterChangeListener) decoration).onAdapterChanged(mAdapter);
                }
            }
        }
    }

    private class ChangeObserver extends AdapterDataObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            ensureEmptyView();
            ensureAdapterChange();
        }
    }

    public interface IStateAdapter {
        void onAttach();

        void onDetach();
    }

    public interface ISizeReceiveAdapter {
        void onSizeChange(int w, int h);
    }

    public interface ItemAppearAnimator {
        void animate(View v, int position, boolean out);
    }

    public interface OnAdapterChangeListener {
        void onAdapterChanged(Adapter adapter);
    }

    public interface IHeaderAdapter {
        boolean hasHeader(int position);

        CharSequence getText(int position);
    }

}
