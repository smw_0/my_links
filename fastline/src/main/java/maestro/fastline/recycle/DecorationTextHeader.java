package maestro.fastline.recycle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import maestro.fastline.R;

/**
 * Created by maestro123 on 02.06.2016.
 */
public class DecorationTextHeader {

    public static final String TAG = DecorationTextHeader.class.getSimpleName();

    private static final int ALIGNMENT_CENTER = 0;
    private static final int ALIGNMENT_LEFT = 1;
    private static final int ALIGNMENT_RIGHT = 2;

    private static final int BACKGROUND_TYPE_FILL = 0;
    private static final int BACKGROUND_TYPE_ROUND = 1;

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int mTextColor = Color.WHITE;
    private int mBackgroundColor = Color.TRANSPARENT;

    private int mHeight = -1;
    private int mPaddingTop;
    private int mPaddingBottom;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mAlignment;
    private int mBackgroundType = BACKGROUND_TYPE_ROUND;

    private float mRadius;

    private boolean isDebug = true;
    private boolean isAllCaps = false;
    private boolean isHaveRelativeRadius = true;

    public DecorationTextHeader(Context context, int style) {
        TypedArray attrs = context.obtainStyledAttributes(style == -1 ? R.style.DefaultTextDecorationStyle : style, R.styleable.TextHeaderDrawer);
        if (attrs.hasValue(R.styleable.TextHeaderDrawer_textSize)) {
            mPaint.setTextSize(attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_textSize, 12));
        }
        if (attrs.hasValue(R.styleable.TextHeaderDrawer_textColor)) {
            mTextColor = attrs.getColor(R.styleable.TextHeaderDrawer_textColor, Color.WHITE);
        }
        if (attrs.hasValue(R.styleable.TextHeaderDrawer_backgroundColor)) {
            mBackgroundColor = attrs.getColor(R.styleable.TextHeaderDrawer_backgroundColor, Color.TRANSPARENT);
        }
        if (attrs.hasValue(R.styleable.TextHeaderDrawer_font)) {
            try {
                mPaint.setTypeface(Typeface.createFromAsset(context.getAssets(), attrs.getString(R.styleable.TextHeaderDrawer_font)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mHeight = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_headerHeight, mHeight);
        mRadius = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_backgroundRadius, 0);
        mAlignment = attrs.getInt(R.styleable.TextHeaderDrawer_alignment, mAlignment);
        mBackgroundType = attrs.getInt(R.styleable.TextHeaderDrawer_backgroundType, mBackgroundType);
        isAllCaps = attrs.getBoolean(R.styleable.TextHeaderDrawer_textAllCaps, isAllCaps);

        mPaddingLeft = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_paddingLeft, 0);
        mPaddingTop = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_paddingTop, 0);
        mPaddingRight = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_paddingRight, 0);
        mPaddingBottom = attrs.getDimensionPixelSize(R.styleable.TextHeaderDrawer_paddingBottom, 0);

        mPaint.setTextAlign(Paint.Align.LEFT);

        attrs.recycle();
    }

    public void draw(Canvas canvas, Rect bounds, CharSequence text, int position) {
        draw(canvas, bounds, text, position, null);
    }

    public void draw(Canvas canvas, Rect bounds, CharSequence text, int position, DecorationHeaderDrawCallback callback) {
        String drawText = String.valueOf(text);
        if (isAllCaps) {
            drawText = drawText.toUpperCase();
        }
        Rect textBounds = new Rect();
        mPaint.getTextBounds(drawText, 0, drawText.length(), textBounds);

        int width = bounds.width();
        int height = bounds.height();

        //NOT ALIGNED !!!
        RectF backgroundRect = null;
        float drawRadius = 0f;

        if (mBackgroundType == BACKGROUND_TYPE_ROUND) {
            backgroundRect = new RectF(0, 0, textBounds.width() + mPaddingLeft + mPaddingRight, textBounds.height() + mPaddingTop + mPaddingBottom);
            backgroundRect.offsetTo((width >> 1) - backgroundRect.width() / 2, (height >> 1) - backgroundRect.height() / 2);
            drawRadius = isHaveRelativeRadius ? backgroundRect.height() / 2 : mRadius;
        } else if (mBackgroundType == BACKGROUND_TYPE_FILL) {
            backgroundRect = new RectF(0, 0, bounds.width(), bounds.height());
        }

        canvas.save();
        if (isDebug) {
            Paint paint = new Paint();
            paint.setColor(Color.RED);
            canvas.drawRect(bounds, paint);
        }

        canvas.translate(0, bounds.top);

        mPaint.setColor(mBackgroundColor);
        canvas.drawRoundRect(backgroundRect, drawRadius, drawRadius, mPaint);

        if (callback != null) {
            int color = callback.getHeaderColor(position);
            mPaint.setColor(color);

            Drawable drawable = callback.getHeaderIcon(position);
            if (drawable != null) {
                if (callback.filterIcons()) {
                    drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                }
                int left = 0;
                int right = left + mPaddingLeft;
                int top = (height >> 1) - (drawable.getIntrinsicHeight() >> 1);
                int bottom = top + drawable.getIntrinsicHeight();
                drawable.setBounds((right >> 1) - (drawable.getIntrinsicWidth() >> 1), top, (right >> 1) + (drawable.getIntrinsicWidth() >> 1), bottom);
                drawable.draw(canvas);
            }
        } else {
            mPaint.setColor(mTextColor);
        }

        if (mAlignment == ALIGNMENT_CENTER) {
            int xPos = (int) backgroundRect.centerX() - (textBounds.width() >> 1);
            int yPos = (int) (backgroundRect.centerY() - ((mPaint.descent() + mPaint.ascent()) / 2));
            canvas.drawText(drawText, 0, drawText.length(), xPos, yPos, mPaint);
        } else {
            canvas.drawText(drawText, 0, drawText.length(), mPaddingLeft,
                    (height >> 1) - (textBounds.height() >> 1) + mPaddingTop, mPaint);
        }

        canvas.restore();

        if (isDebug) {
            Paint paint = new Paint();
            paint.setColor(Color.GREEN);
            canvas.drawLine(0, bounds.centerY(), bounds.right, bounds.centerY(), paint);
        }

    }

    public int getHeight() {
        return mHeight;
    }

    public interface DecorationHeaderDrawCallback {

        int getHeaderColor(int position);

        Drawable getHeaderIcon(int position);

        boolean filterIcons();

    }

}
