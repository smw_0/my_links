package maestro.fastline.recycle;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Artyom on 08.03.2016.
 */
public class BaseSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    public RecyclerView mList;

    public BaseSpanSizeLookup(RecyclerView list) {
        mList = list;
    }

    @Override
    public int getSpanSize(int position) {
        RecyclerView.LayoutManager manager = mList.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            int itemType = mList.getAdapter().getItemViewType(position);
            return itemType == BaseRecyclerAdapter.TYPE_HEADER
                    || itemType == BaseRecyclerAdapter.TYPE_FOOTER
                    ? ((GridLayoutManager) manager).getSpanCount()
                    : 1;
        }
        return 1;
    }
}