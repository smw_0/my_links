package maestro.fastline.recycle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import maestro.fastline.R;

/**
 * Created by maestro123 on 25.05.2016.
 */
public class SpaceItemDecoration extends DividerItemDecoration {

    private int mSpaceEnd;
    private int mSpanCount;
    private int mSpaceSize;
    private int mSpaceStart;
    private int mStartEdgeSpace;
    private int mEndSpaceEdge;
    private int mTypeChangeSpace;

    private boolean isHorizontal;
    private boolean isIncludeEdge;
    private boolean isDrawDividers;

    private ColumnLookup mColumnLookup;

    public static SpaceItemDecoration create(Context context, int styleResource) {
        return create(context, 1, styleResource);
    }

    public static SpaceItemDecoration create(Context context, int spanCount, int styleResource) {
        SpaceItemDecoration decoration = new SpaceItemDecoration(context);
        decoration.setStyle(context, spanCount, styleResource);
        return decoration;
    }

    public SpaceItemDecoration(Context context) {
        super(context);
    }

    public SpaceItemDecoration(Context context, int resId) {
        super(context, resId);
    }

    public SpaceItemDecoration setStyle(Context context, int spanCount, int styleResource) {
        mSpanCount = spanCount;
        TypedArray array = context.obtainStyledAttributes(styleResource, R.styleable.SpaceDecoration);
        mSpaceStart = array.getDimensionPixelSize(R.styleable.SpaceDecoration_startSpace, 0);
        mSpaceEnd = array.getDimensionPixelSize(R.styleable.SpaceDecoration_endSpace, 0);
        mSpaceSize = array.getDimensionPixelSize(R.styleable.SpaceDecoration_space, 0);
        mStartEdgeSpace = array.getDimensionPixelSize(R.styleable.SpaceDecoration_startEdgeSpace, mStartEdgeSpace);
        mEndSpaceEdge = array.getDimensionPixelSize(R.styleable.SpaceDecoration_endEdgeSpace, mEndSpaceEdge);
        mTypeChangeSpace = array.getDimensionPixelSize(R.styleable.SpaceDecoration_typeChangeSpace, mTypeChangeSpace);
        isHorizontal = array.getBoolean(R.styleable.SpaceDecoration_horizontal, false);
        isIncludeEdge = array.getBoolean(R.styleable.SpaceDecoration_includeEdge, true);
        isDrawDividers = array.getBoolean(R.styleable.SpaceDecoration_drawDividers, true);
        array.recycle();
        return this;
    }

    public void setColumnLookup(ColumnLookup lookup) {
        mColumnLookup = lookup;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        RecyclerView.Adapter adapter = parent.getAdapter();
        int position = parent.getChildAdapterPosition(view);
        boolean isFirstRow = mColumnLookup != null ? mColumnLookup.isFirstRow(adapter, position, mSpanCount) : position < mSpanCount;
        boolean isLastRow = mColumnLookup != null ? mColumnLookup.isLastRow(adapter, position, mSpanCount) : position >= adapter.getItemCount() - mSpanCount;
        if (isFirstRow) {
            if (isHorizontal) {
                outRect.left = mSpaceStart;
            } else {
                outRect.top = mSpaceStart;
            }
        }
        if (isLastRow) {
            if (isHorizontal) {
                outRect.right = mSpaceEnd;
            } else {
                outRect.bottom = mSpaceEnd;
            }
        }
        if (mSpaceSize > 0) {
            if (mSpanCount == 1 && isIncludeEdge) {
                if (isHorizontal) {
                    outRect.top = mStartEdgeSpace;
                    outRect.bottom = mEndSpaceEdge;
                    if (!isLastRow) {
                        outRect.right = mSpaceSize;
                    }
                } else {
                    outRect.left = mStartEdgeSpace;
                    outRect.right = mEndSpaceEdge;
                    if (!isLastRow) {
                        outRect.bottom = mSpaceSize;
                    }
                }
            } else {
                int halfSpace = mSpaceSize >> 2;
                boolean isFirstInRow = false;
                boolean isLastInRow = false;
                boolean horizontalSpaceAllow = true;
                if (mColumnLookup != null) {
                    isFirstInRow = mColumnLookup.isFirstInRow(adapter, position, mSpanCount);
                    isLastInRow = mColumnLookup.isLastInRow(adapter, position, mSpanCount);
                    horizontalSpaceAllow = mColumnLookup.isHorizontalSpaceAllow(adapter, position, mSpanCount);
                } else {
                    isFirstInRow = position % mSpanCount == 0;
                    isLastInRow = position % mSpanCount == mSpanCount - 1;
                }
                if (isFirstInRow) {
                    outRect.left = mStartEdgeSpace;
                } else {
                    outRect.left = mStartEdgeSpace >> 1;
                }
                if (isLastInRow) {
                    outRect.right = mEndSpaceEdge;
                } else {
                    outRect.right = mEndSpaceEdge >> 1;
                }
                if (horizontalSpaceAllow && !isLastRow) {
                    outRect.bottom = mSpaceSize;
                }
            }
        }
    }

    public SpaceItemDecoration setSpaceStar(int size) {
        mSpaceStart = size;
        return this;
    }

    public SpaceItemDecoration setSpaceEnd(int size) {
        mSpaceEnd = size;
        return this;
    }

    public int getSpaceStart() {
        return mSpaceStart;
    }

    public int getSpaceEnd() {
        return mSpaceEnd;
    }

    public int getSpaceSize() {
        return mSpaceSize;
    }

    @Override
    protected boolean canDraw(View view, RecyclerView recyclerView) {
        return isDrawDividers;
    }

    public interface ColumnLookup<Adapter extends RecyclerView.Adapter> {
        boolean isFirstRow(Adapter adapter, int adapterPosition, int spanCount);

        boolean isLastRow(Adapter adapter, int adapterPosition, int spanCount);

        boolean isFirstInRow(Adapter adapter, int adapterPosition, int spanCount);

        boolean isLastInRow(Adapter adapter, int adapterPosition, int spanCount);

        boolean isHorizontalSpaceAllow(Adapter adapter, int adapterPosition, int spanCount);
    }

}
