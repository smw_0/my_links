package maestro.fastline.recycle;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Artyom on 25.02.2016.
 */
public class BasePlaceViewHolder extends BaseViewHolder {

    public static BasePlaceViewHolder create(Context context) {
        return new BasePlaceViewHolder(new FrameLayout(context));
    }

    public FrameLayout Frame;
    public View ViewForPlace;

    private FrameLayout.LayoutParams mParams
            = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);

    public BasePlaceViewHolder(View v) {
        super(v);
        Frame = (FrameLayout) v;
    }

    public void bindView(View view) {
        ViewForPlace = view;
        if (ViewForPlace != null) {
            if (ViewForPlace.getParent() != null
                    && ViewForPlace.getParent() instanceof ViewGroup) {
                ViewGroup parentGroup = (ViewGroup) ViewForPlace.getParent();
                parentGroup.removeView(ViewForPlace);
            }
            Frame.removeAllViews();
            Frame.addView(view, mParams);
        }
    }

    public FrameLayout.LayoutParams getLayoutParams() {
        return mParams;
    }

}
