package maestro.fastline.recycle;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Artyom on 17.02.2016.
 */
public class BaseViewHolder<DataType> extends RecyclerView.ViewHolder {

    public static final String TAG = BaseViewHolder.class.getSimpleName();

    public View ItemView;
    private DataType mDataObject;

    private long mId;
    private int mItemPosition;

    public BaseViewHolder(View v) {
        super(v);
        ItemView = v;
        v.setTag(this);
    }

    public void onCreate() {
    }

    public void setItemPosition(int position) {
        mItemPosition = position;
    }

    public int getItemPosition() {
        return mItemPosition;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setData(DataType data) {
        mDataObject = data;
    }

    public void forceSetData(DataType data) {
        setData(data);
        applyData(data);
    }

    public void forceApplyData() {
        applyData(mDataObject);
    }

    public void applyData(DataType dataType) {
        if (dataType == null) {
            onEmptyDataAttach();
        } else {
            onDataAttach(dataType);
        }
    }

    public void onDataAttach(DataType data) {
    }

    public void onEmptyDataAttach() {
    }

    public DataType getAttachedData() {
        return mDataObject;
    }

}
