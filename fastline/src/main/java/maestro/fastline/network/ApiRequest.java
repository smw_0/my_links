package maestro.fastline.network;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;

import maestro.fastline.FastlineUtils;

/**
 * Created by maestro123 on 22.06.2016.
 */
public class ApiRequest implements Runnable {

    public static final String TAG = ApiRequest.class.getSimpleName();

    public static ApiRequest create(Uri.Builder builder) {
        return create(builder, Method.Get);
    }

    public static ApiRequest create(Uri.Builder builder, Method method) {
        return create(builder, method, null);
    }

    public static ApiRequest create(Uri.Builder builder, Method method, String postData) {
        return new ApiRequest(builder.build().toString(), method, postData);
    }

    private Object mTag;
    private String mUrl;
    private String mData;
    private Method mMethod;
    private ApiError mError;

    private Object mResult;
    private JSONObject mJsonResult;
    private HttpClient mCurrentClient;

    private SoftReference<OnRequestEventListener> mListenerReference;

    private boolean isCanceled = false;
    private boolean isCancelable = true;

    private static final HashMap<String, String> mForceResponses = new HashMap<>();
    private static boolean isDebug = false;

    public static void addForceResponse(String key, String response) {
        mForceResponses.put(key, response);
    }

    public static void setDebug(boolean debug) {
        isDebug = debug;
    }

    public ApiRequest(String url) {
        this(url, Method.Get);
    }

    public ApiRequest(String url, Method method) {
        this(url, method, null);
    }

    public ApiRequest(String url, Method method, String postData) {
        mUrl = url;
        mMethod = method;
        mData = postData;
    }

    public void setPostData(String data) {
        mData = data;
    }

    @Override
    public void run() {
        execute();
    }

    public void async(OnRequestEventListener listener) {
        setListener(listener);
        async();
    }

    public void async() {
        ExecuteManager.Get().submit(this);
    }

    public void asyncSingle(OnRequestEventListener listener) {
        setListener(listener);
        async();
    }

    public void asyncSingle() {
        ExecuteManager.Get().submitSingle(this);
    }

    public <D> AsyncTaskLoader<D> loader(Context context) {
        return new ApiRequestLoader<D>(context, this);
    }

    public <D> AsyncTaskLoader<D> loader(Context context, OnRequestEventListener listener) {
        setListener(listener);
        return new ApiRequestLoader<D>(context, this);
    }

    public <T extends ApiRequest> T setListener(OnRequestEventListener listener) {
        mListenerReference = new SoftReference<OnRequestEventListener>(listener);
        return (T) this;
    }

    public <T extends ApiRequest> T setTag(Object tag) {
        mTag = tag;
        return (T) this;
    }

    public Object getTag() {
        return mTag;
    }

    public Method getMethod() {
        return mMethod;
    }

    public void setError(ApiError error) {
        mError = error;
    }

    public ApiError getError() {
        return mError;
    }

    public JSONObject getJsonResult() {
        return mJsonResult;
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void cancel() {
        if (isCancelable) {
            isCanceled = true;
            if (mCurrentClient != null && mCurrentClient.getConnectionManager() != null) {
                mCurrentClient.getConnectionManager().shutdown();
            }
            if (mListenerReference != null) {
                OnRequestEventListener listener = mListenerReference.get();
                if (listener != null) {
                    listener.onRequestCancel(this);
                }
            }
        }
    }

    public <T extends ApiRequest> T setCancelable(boolean cancelable) {
        isCancelable = cancelable;
        return (T) this;
    }

    public Object execute() {
        try {
            if (isCanceled) {
                return null;
            }
            ExecuteManager.Get().addProcessRequest(this);
            OnRequestEventListener listener = getListener();
            if (listener != null) {
                listener.onRequestStart(this);
            }
            try {

                HttpClient client = obtainClient();
                HttpRequestBase request = null;
                switch (mMethod) {
                    case Get: {
                        request = new HttpGet(mUrl);
                        break;
                    }
                    case Delete: {
                        request = new HttpDelete(mUrl);
                        break;
                    }
                    case Post: {
                        HttpPost postRequest = new HttpPost(mUrl);
                        onWriteData(postRequest, mData);
                        request = postRequest;
                        break;
                    }
                    case Patch: {
                        HttpPatch patchRequest = new HttpPatch(mUrl);
                        onWriteData(patchRequest, mData);
                        request = patchRequest;
                        break;
                    }
                    case Put: {
                        HttpPut putRequest = new HttpPut(mUrl);
                        onWriteData(putRequest, mData);
                        request = putRequest;
                        break;
                    }
                }
                prepareConnection(client, request);
                if (mError != null) {
                    return null;
                }

                HttpResponse response = client.execute(request);
                InputStream responseStream = response.getEntity().getContent();
                if (response.getStatusLine().getStatusCode() == 200) {
                    if (onStreamAppear(response, responseStream)) {
                        return null;
                    }
                }
                try {
                    if (isDebug) {
                        for (String key : mForceResponses.keySet()) {
                            if (mUrl.contains(key)) {
                                mJsonResult = new JSONObject(mForceResponses.get(key));
                            }
                        }
                        if (mJsonResult == null) {
                            mJsonResult = new JSONObject(FastlineUtils.sts(responseStream));
                        }
                    } else {
                        mJsonResult = new JSONObject(FastlineUtils.sts(responseStream));
                    }
                    mResult = processResponse(mJsonResult);
                    resolveErrorIfAllow(mError);
                    Log.e(TAG, "req: " + mUrl + ",\ndata: " + mData + ",\nres: " + mJsonResult);
                    return mResult;
                } catch (JSONException e) {
                    e.printStackTrace();
                    mError = ApiError.Server;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (SocketTimeoutException | ConnectException | UnknownHostException e) {
                e.printStackTrace();
                mError = ApiError.Connection;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mError == null) {
                mError = ApiError.Unknown;
            }
            resolveErrorIfAllow(mError);
            return null;
        } finally {
            if (!isCanceled() || mResult != null) {
                OnRequestEventListener listener = getListener();
                if (listener != null) {
                    try {
                        if (mError != null) {
                            listener.onRequestFail(this, mError);
                        } else {
                            try {
                                listener.onRequestFinishSuccessful(this, mResult);
                            } catch (Exception e) {
                                e.printStackTrace();
                                listener.onRequestFail(this, ApiError.Unknown);
                            }
                        }
                    } finally {
                        listener.release(this);
                    }
                }
            }
            ExecuteManager.Get().removeProcessRequest(this);
        }
    }

    public void prepareConnection(HttpClient client, HttpRequestBase requestBase) {
    }

    public boolean onStreamAppear(HttpResponse response, InputStream stream) {
        return false;
    }

    public Object processResponse(JSONObject jsonObject) {
        return jsonObject;
    }

    private void resolveErrorIfAllow(ApiError error) {
        if (error != null) {
            resolveErrorError(error);
        }
    }

    public boolean resolveErrorError(ApiError error) {
        return false;
    }

    public void onWriteData(HttpEntityEnclosingRequestBase request, String data) throws Exception {
//        try {
            request.setEntity(new ByteArrayEntity(mData.getBytes()));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private OnRequestEventListener getListener() {
        if (mListenerReference != null) {
            return mListenerReference.get();
        }
        return null;
    }

    protected HttpClient obtainClient() {
        return new DefaultHttpClient();
    }

    public enum Method {
        Get("GET"), Post("POST"), Patch("PATCH"), Delete("DELETE"), Put("PUT");

        private String mMethod;

        Method(String method) {
            mMethod = method;
        }

        public String getMethod() {
            return mMethod;
        }
    }

    public interface OnRequestEventListener<T> {
        void onRequestStart(ApiRequest request);

        void onRequestFinishSuccessful(ApiRequest request, T result);

        void onRequestFail(ApiRequest request, ApiError error);

        void onRequestCancel(ApiRequest request);

        void release(ApiRequest request);
    }

    public static class SimpleRequestEventListener<T> implements OnRequestEventListener<T> {

        @Override
        public void onRequestStart(ApiRequest request) {

        }

        @Override
        public void onRequestFinishSuccessful(ApiRequest request, T result) {

        }

        @Override
        public void onRequestFail(ApiRequest request, ApiError error) {

        }

        @Override
        public void onRequestCancel(ApiRequest request) {

        }

        @Override
        public void release(ApiRequest request) {

        }
    }

}
