package maestro.fastline.network;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Artyom on 10.07.2016.
 */
public class MultipartBuilder {

    public static final String TAG = MultipartBuilder.class.getSimpleName();

    private static final String LINE_FEED = "\r\n";

//    private final String boundary;

    private PrintWriter writer;
    private OutputStream outputStream;

//    public MultipartBuilder(HttpURLConnection connection)
//            throws IOException {
//
//        // creates a unique boundary based on time stamp
//        boundary = "===" + System.currentTimeMillis() + "===";
//
//        connection.setUseCaches(false);
//        connection.setDoOutput(true); // indicates POST method
//        connection.setDoInput(true);
//        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
//        outputStream = connection.getOutputStream();
//        writer = new PrintWriter(new OutputStreamWriter(outputStream), true);
//    }
//
//    public void addFormField(String name, String value) {
//        writer.append("--").append(boundary).append(LINE_FEED);
//        writer.append("Content-Disposition: form-data; name=\"").append(name).append("\"").append(LINE_FEED);
//        writer.append(LINE_FEED);
//        writer.append(value).append(LINE_FEED);
//        writer.flush();
//    }
//
//    public void addFilePart(String fieldName, String fileName, InputStream stream, AtomicBoolean cancelCallback, final FlipApi.DriveProgressListener listener, long fileSize)
//            throws IOException {
//        writer.append("--").append(boundary).append(LINE_FEED);
//        writer.append("Content-Disposition: form-data; name=\"").append(fieldName).append("\"; filename=\"").append(fileName).append("\"")
//                .append(LINE_FEED);
//        writer.append("Content-Type: ").append(URLConnection.guessContentTypeFromName(fileName))
//                .append(LINE_FEED);
//        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
//        writer.append(LINE_FEED);
//        writer.flush();
//
//        FlipUtils.write(stream, outputStream, new FlipApi.DriveProgressListener() {
//            @Override
//            public void onProgressChange(long current, long total) {
//                if (listener != null){
//                    listener.onProgressChange(current, total);
//                }
//                Log.e(TAG, "onChunkUploadProgress: " + (current / (float) total));
//            }
//        }, cancelCallback, fileSize);
//
//        outputStream.flush();
//
////        writer.append(LINE_FEED);
//        writer.flush();
//    }
//
//    public void addHeaderField(String name, String value) {
//        writer.append(name).append(": ").append(value).append(LINE_FEED);
//        writer.flush();
//    }
//
//    public void finish() throws IOException {
//        writer.append(LINE_FEED).flush();
//        writer.append("--").append(boundary).append("--").append(LINE_FEED);
//        writer.close();
//    }
}
