package maestro.fastline.network;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by maestro123 on 12.05.2016.
 */
public class ApiError implements Serializable {

    public static ApiError Connection = new ApiError("TODO: no internet connection title", "TODO: no internet connection message");
    public static ApiError Unknown = new ApiError("TODO: unknown error title", "TODO: unknown error message");
    public static ApiError Server = new ApiError("TODO: server error title", "TODO: server error message");

    public static void setConnectionText(int title, int message) {
        Connection.mErrorTitle = title;
        Connection.mErrorMessage = message;
    }

    public static void setUnknownText(int title, int message) {
        Unknown.mErrorTitle = title;
        Unknown.mErrorMessage = message;
    }

    public static void setServerText(int title, int message) {
        Server.mErrorTitle = title;
        Server.mErrorMessage = message;
    }

    public static final int NOT_SET = -1;

    @SerializedName("status")
    private String mStatus;
    @SerializedName("error_code")
    private String mReason;

    @SerializedName("parameter")
    private String mParameter;

    @SerializedName("attribute")
    private String mAttribute;

    @SerializedName("error_title")
    private int mErrorTitle = NOT_SET;
    @SerializedName("error_message")
    private int mErrorMessage = NOT_SET;

    private JSONObject mOriginJson;

    public ApiError(int errorTitle, int errorMessage) {
        mErrorTitle = errorTitle;
        mErrorMessage = errorMessage;
    }

    public ApiError(String status) {
        mStatus = status;
    }

    public ApiError(String status, String reason) {
        mStatus = status;
        mReason = reason;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getReason() {
        return mReason;
    }

    public void setErrorTitle(int errorTitle) {
        this.mErrorTitle = errorTitle;
    }

    public int getErrorTitle() {
        return mErrorTitle;
    }

    public void setErrorMessage(int errorMessage) {
        this.mErrorMessage = errorMessage;
    }

    public int getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrors(int errorTitle, int errorMessage) {
        mErrorTitle = errorTitle;
        mErrorMessage = errorMessage;
    }

    public void setOriginJson(JSONObject originJson) {
        mOriginJson = originJson;
    }

    public JSONObject getOriginJson() {
        return mOriginJson;
    }

    public void setParameter(String parameter) {
        mParameter = parameter;
    }

    public String getParameter() {
        return mParameter;
    }

    public void setAttribute(String attribute) {
        mAttribute = attribute;
    }

    public String getAttribute() {
        return mAttribute;
    }

    public boolean isParameterEquals(String parameter) {
        return !TextUtils.isEmpty(parameter) && parameter.equals(mParameter);
    }

    public boolean isAttributeEquals(String attribute) {
        return !TextUtils.isEmpty(attribute) && attribute.equals(mAttribute);
    }

    public boolean isConnection() {
        return this == Connection;
    }

    public boolean isUnknown() {
        return this == Unknown;
    }

    public String getErrorTitle(Context context) {
        return mErrorTitle != NOT_SET ? context.getString(mErrorTitle) : "";
    }

    public String getErrorMessage(Context context) {
        return mErrorMessage != NOT_SET ? context.getString(mErrorMessage) : "";
    }

    public boolean is(String errorStatus) {
        if (errorStatus == null) {
            return false;
        }
        return errorStatus.equals(mStatus);
    }

    public boolean isReason(String errorReason) {
        if (errorReason == null) {
            return false;
        }
        return errorReason.equals(mReason);
    }

    public void from(ApiError error){
        setErrorTitle(error.getErrorTitle());
        setErrorMessage(error.getErrorMessage());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "[status:" + mStatus + ", reason: " + mReason + "]";
    }
}
