package maestro.fastline.network;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by maestro123 on 04.07.2016.
 */
public class ApiRequestLoader<D> extends AsyncTaskLoader<D> {

    private ApiRequest mRequest;

    public ApiRequestLoader(Context context, ApiRequest request) {
        super(context);
        mRequest = request;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public D loadInBackground() {
        Object result = mRequest.execute();
        if (mRequest.isCanceled()) {
            reset();
            abandon();
            return null;
        } else if (mRequest.getError() != null) {
            return (D) mRequest.getError();
        }
        return onSuccessLoad((D) result);
    }

    protected D onSuccessLoad(D result) {
        return result;
    }

}
