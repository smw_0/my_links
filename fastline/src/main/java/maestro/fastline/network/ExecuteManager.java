package maestro.fastline.network;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maestro123 on 09.07.2016.
 */
public final class ExecuteManager {

    private static volatile ExecuteManager instance;

    public static synchronized ExecuteManager Get() {
        if (instance == null) {
            synchronized (ExecuteManager.class) {
                if (instance == null) {
                    instance = new ExecuteManager();
                }
            }
        }
        return instance;
    }

    private ExecutorService mSingleService;
    private ExecutorService mThreadingService;

    private final ArrayList<ApiRequest> mProcessRequests = new ArrayList<>();
    private final Object mExecutorLock = new Object();

    ExecuteManager() {
        obtainExecutors();
    }

    public void submit(ApiRequest request) {
        synchronized (mExecutorLock) {
            mThreadingService.submit(request);
        }
    }

    public void submitSingle(ApiRequest request) {
        synchronized (mExecutorLock) {
            mSingleService.submit(request);
        }
    }

    public void addProcessRequest(ApiRequest apiRequest) {
        synchronized (mProcessRequests) {
            mProcessRequests.add(apiRequest);
        }
    }

    public void removeProcessRequest(ApiRequest apiRequest) {
        synchronized (mProcessRequests) {
            mProcessRequests.remove(apiRequest);
        }
    }

    public void cancelAll() {
        synchronized (mProcessRequests) {
            for (ApiRequest apiRequest : mProcessRequests) {
                apiRequest.cancel();
            }
        }
        synchronized (mExecutorLock) {
            mSingleService.shutdownNow();
            mThreadingService.shutdownNow();
            obtainExecutors();
        }
    }

    private void obtainExecutors() {
        synchronized (mExecutorLock) {
            mSingleService = Executors.newSingleThreadExecutor();
            mThreadingService = Executors.newFixedThreadPool(3);
        }
    }

}
