package maestro.fastline.data;

import android.text.TextUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

/**
 * Created by maestro123 on 04.07.2016.
 */
public class SqlUtils {

    public static boolean isLong(Field field) {
        return (field.getType().isPrimitive() && field.getType().equals(long.class)) || Long.class.equals(field.getType());
    }

    public static boolean isDouble(Field field) {
        return (field.getType().isPrimitive() && field.getType().equals(double.class)) || Double.class.equals(field.getType());
    }

    public static boolean isFloat(Field field) {
        return (field.getType().isPrimitive() && field.getType().equals(float.class)) || Float.class.equals(field.getType());
    }

    public static boolean isInteger(Field field) {
        return (field.getType().isPrimitive() && field.getType().equals(int.class)) || Integer.class.equals(field.getType());
    }

    public static boolean isBoolean(Field field) {
        return (field.getType().isPrimitive() && field.getType().equals(boolean.class)) || Boolean.class.equals(field.getType());
    }

    public static boolean isText(Field field) {
        return String.class.equals(field.getType()) || CharSequence.class.equals(field.getType());
    }

    public static boolean isArray(Field field) {
        return field.getType().isArray();
    }

    public static boolean isCollection(Field field) {
        return Collection.class.isAssignableFrom(field.getType());
    }

    public static boolean isEnum(Field field) {
        return field.getType().isEnum();
    }

    public static boolean isBooleanInstance(Class<?> cls) {
        return cls.equals(boolean.class) || Boolean.class.equals(cls);
    }

    public static boolean isNullOrEmpty(Object... objects) {
        return objects == null || objects.length == 0;
    }

    public static boolean isNullOrEmpty(List objects) {
        return objects == null || objects.size() == 0;
    }

    public static String normalizeString(String source) {
        return TextUtils.isEmpty(source) ? source : source.replaceAll("'", "&apostraphe;");
    }

    public static String backwardString(String source) {
        return TextUtils.isEmpty(source) ? source : source.replaceAll("&apostraphe;", "'");
    }

    public static String toSqlStatementValue(Object value) {
        if (value instanceof String) {
            return "'" + normalizeString((String) value) + "'";
        } else if (value == null) {
            return "NULL";
        } else if (isBooleanInstance(value.getClass())) {
            return ((Boolean) value) ? "1" : "0";
        } else {
            return "'" + String.valueOf(value) + "'";
        }
    }

    public static boolean isPrimitiveNull(Field field, Object value) {
        if (isLong(field) && ((Long) value) == 0) {
            return true;
        } else if (isInteger(field) && ((Integer) value) == 0) {
            return true;
        }
        return false;
    }

    public static String[] convertToStringArray(Object... objects) {
        final int size = objects.length;
        final String[] strObjects = new String[objects.length];
        for (int i = 0; i < size; i++) {
            strObjects[i] = String.valueOf(objects[i]);
        }
        return strObjects;
    }

}
