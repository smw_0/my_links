package maestro.fastline.data;

/**
 * Created by maestro123 on 09.07.2016.
 */
public class JsonBuilder {

    public static final String BRACKET_START = "{";
    public static final String BRACKET_END = "}";
    public static final String ARRAY_BRACKET_LEFT = "[";
    public static final String ARRAY_BRACKET_RIGHT = "]";
    public static final String COMMA = ",";
    public static final String COLON = ":";

    public static final String EMPTY = "";

    private static final String ATTR_TEMPLATE = "\"%s\"";

    private StringBuilder mBuilder;
    private boolean isClosed = false;
    private boolean isFirstAttr = true;

    private boolean isInArray = false;
    private boolean isInArrayObject = false;
    private boolean isFirstArrayAttr = false;
    private boolean isFirstArrayObjectAttr = false;

    public static JsonBuilder Create() {
        return new JsonBuilder();
    }

    public static JsonBuilder Create(String sourceJson) {
        String initialValue = sourceJson;
        int lastCloseIndex = sourceJson.lastIndexOf("}");
        if (lastCloseIndex > -1) {
            initialValue = sourceJson.substring(0, lastCloseIndex);
        }
        JsonBuilder builder = new JsonBuilder();
        builder.mBuilder.append(initialValue);
        builder.isFirstAttr = false;
        return builder;
    }

    JsonBuilder() {
        mBuilder = new StringBuilder();
        mBuilder.append(BRACKET_START);
    }

    public JsonBuilder Value(String attr, Object value) {
        if (value != null) {
            onNewValue();
            mBuilder.append(String.format(ATTR_TEMPLATE, attr))
                    .append(COLON);
            write(value);
        }
        return this;
    }

    public JsonBuilder ValueOrValue(String attr, Object firstValue, Object secondValue) {
        if (firstValue != null) {
            return Value(attr, firstValue);
        }
        return Value(attr, secondValue);
    }

    public JsonBuilder Array(String attr) {
        closeArray();
        onNewValue();
        mBuilder.append(String.format(ATTR_TEMPLATE, attr))
                .append(COLON)
                .append(ARRAY_BRACKET_LEFT);
        isInArray = true;
        isFirstArrayAttr = true;
        return this;
    }

    public JsonBuilder ArrayValue(Object value) {
        if (value != null) {
            onNewValue();
            write(value);
        }
        return this;
    }

    public JsonBuilder ArrayObject() {
        closeArrayObject();
        onNewValue();
        mBuilder.append(BRACKET_START);
        isInArrayObject = true;
        isFirstArrayObjectAttr = true;
        return this;
    }

    public void CloseArray() {
        closeArray();
    }

    public String build() {
        if (!isClosed) {
            mBuilder.append(BRACKET_END);
            isClosed = true;
        }
        return mBuilder.toString();
    }

    private void onNewValue() {
        boolean isFirst = isInArray ? isInArrayObject ? isFirstArrayObjectAttr : isFirstArrayAttr : isFirstAttr;
        if (!isFirst) {
            mBuilder.append(COMMA);
        } else {
            if (isInArray) {
                if (isInArrayObject) {
                    isFirstArrayObjectAttr = false;
                } else {
                    isFirstArrayAttr = false;
                }
            } else {
                isFirstAttr = false;
            }
        }
    }

    private void closeArray() {
        closeArrayObject();
        if (isInArray) {
            mBuilder.append(ARRAY_BRACKET_RIGHT);
            isInArray = false;
            isFirstArrayAttr = false;
        }
    }

    private void closeArrayObject() {
        if (isInArrayObject) {
            mBuilder.append(BRACKET_END);
            isInArrayObject = true;
            isFirstArrayObjectAttr = false;
        }
    }

    private void write(Object object) {
        if (object instanceof CharSequence) {
            writeString((CharSequence) object);
        } else {
            mBuilder.append(String.valueOf(object));
        }
    }

    private void writeString(CharSequence value) {
        mBuilder.append("\"");
        for (int i = 0, length = value.length(); i < length; i++) {
            char c = value.charAt(i);

            /*
             * From RFC 4627, "All Unicode characters may be placed within the
             * quotation marks except for the characters that must be escaped:
             * quotation mark, reverse solidus, and the control characters
             * (U+0000 through U+001F)."
             */
            switch (c) {
                case '"':
                case '\\':
                case '/':
                    mBuilder.append('\\').append(c);
                    break;

                case '\t':
                    mBuilder.append("\\t");
                    break;

                case '\b':
                    mBuilder.append("\\b");
                    break;

                case '\n':
                    mBuilder.append("\\n");
                    break;

                case '\r':
                    mBuilder.append("\\r");
                    break;

                case '\f':
                    mBuilder.append("\\f");
                    break;

                default:
                    if (c <= 0x1F) {
                        mBuilder.append(String.format("\\u%04x", (int) c));
                    } else {
                        mBuilder.append(c);
                    }
                    break;
            }
        }
        mBuilder.append("\"");
    }

}
