package maestro.fastline.data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Created by maestro123 on 18.07.2016.
 */
public class ClassField {

    private Class<?> selfClass;
    private Class<?> superClass;

    private String fieldName;

    public ClassField(Class<?> selfClass, Class<?> superClass, String fieldName) {
        this.selfClass = selfClass;
        this.superClass = superClass;
        this.fieldName = fieldName;
    }

    public Field getField() {
        try {
            return superClass != null ? superClass.getDeclaredField(fieldName) : selfClass.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFieldName() {
        return fieldName;
    }

    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
        return getField().getAnnotation(annotationType);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ClassField){
            ClassField oObject = (ClassField) obj;
            return fieldName.equals(oObject.fieldName);
        }
        return super.equals(obj);
    }
}
