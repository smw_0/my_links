package maestro.fastline.data;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by maestro123 on 04.07.2016.
 */
public class FastlineJson {

    public static final String TAG = FastlineJson.class.getSimpleName();

    public static class JsonKeyHolder {

        private ClassField classField;

        private String jsonName;
        private String alternateJsonName;
        private String sqlColumnName;

        public String getFieldName() {
            return classField.getFieldName();
        }

        public String getJsonName() {
            return jsonName;
        }

        public String getSqlColumnName() {
            return sqlColumnName;
        }
    }

    private static HashMap<Class<?>, HashMap<String, JsonKeyHolder>> cachedColumns = new HashMap<>();


    public static HashMap<String, JsonKeyHolder> getHolders(Class<?> cls) {
        return getHolders(cls, true);
    }

    public static HashMap<String, JsonKeyHolder> getHolders(Class<?> cls, boolean useSqlColumn) {
        HashMap<String, JsonKeyHolder> holders = cachedColumns.get(cls);
        if (holders == null) {
            holders = new HashMap<>();
            ClassField[] fields = collectFields(cls);
            cachedColumns.put(cls, holders);
            for (ClassField field : fields) {
                JsonKey jsonKey = field.getAnnotation(JsonKey.class);
                if (jsonKey != null) {
                    JsonKeyHolder holder = new JsonKeyHolder();
                    holder.classField = field;
                    holder.jsonName = resolveJsonKeyName(jsonKey, field);
                    SqlColumn sqlColumn = field.getAnnotation(SqlColumn.class);
                    if (sqlColumn != null) {
                        holder.sqlColumnName = FastlineSql.resolveColumnName(sqlColumn, field);
                    }
                    holders.put(holder.jsonName, holder);
                    if (!TextUtils.isEmpty(jsonKey.alternate())) {
                        JsonKeyHolder alternateHolder = new JsonKeyHolder();
                        alternateHolder.classField = field;
                        alternateHolder.jsonName = jsonKey.alternate();
                        alternateHolder.sqlColumnName = holder.sqlColumnName;
                        holders.put(alternateHolder.jsonName, alternateHolder);
                    }
                } else if (useSqlColumn) {
                    SqlColumn sqlColumn = field.getAnnotation(SqlColumn.class);
                    if (sqlColumn != null && sqlColumn.isTokenizeKeyLinkable()) {
                        JsonKeyHolder holder = new JsonKeyHolder();
                        holder.classField = field;
                        holder.jsonName = FastlineSql.resolveColumnName(sqlColumn, field);
                        holder.sqlColumnName = holder.jsonName;
                        holders.put(holder.jsonName, holder);
                    }
                }
            }
        }
        return holders;
    }

    public static ClassField[] collectFields(Class<?> cls) {
        ArrayList<ClassField> outFields = new ArrayList<>();
        Field[] fields = cls.getDeclaredFields();
        addFields(outFields, cls, null, fields);

        Class<?> superClass = cls.getSuperclass();
        while (superClass != null && superClass != Object.class) {
            Field[] superFields = superClass.getDeclaredFields();
            if (!SqlUtils.isNullOrEmpty(superFields)) {
                addFields(outFields, cls, superClass, superClass.getDeclaredFields());
            }
            superClass = superClass.getSuperclass();
        }
        return outFields.toArray(new ClassField[outFields.size()]);
    }

    private static void addFields(ArrayList<ClassField> outFields, Class<?> selfClass, Class<?> superClass, Field... fields) {
        for (Field field : fields) {
            outFields.add(new ClassField(selfClass, superClass, field.getName()));
        }
    }

    public static <T> ArrayList<T> fromJson(Class<?> cls, JSONArray jsonArray) {
        final ArrayList<T> outList = new ArrayList<>();
        final int size = jsonArray.length();
        for (int i = 0; i < size; i++) {
            outList.add((T) fromJson(cls, jsonArray.optJSONObject(i)));
        }
        return outList;
    }


    public static <T> T fromJson(Class<?> cls, JSONObject jsonObject) {
        return fromJson(cls, null, jsonObject, true);
    }

    public static <T> T fromJson(Object object, JSONObject jsonObject) {
        return fromJson(object.getClass(), object, jsonObject, true);
    }

    public static <T> T fromJson(Object object, JSONObject jsonObject, boolean useSqlColumn) {
        return fromJson(object.getClass(), object, jsonObject, useSqlColumn);
    }

    public static <T> T fromJson(Class<?> cls, Object object, JSONObject jsonObject) {
        return fromJson(cls, object, jsonObject, true);
    }

    public static <T> T fromJson(Class<?> cls, Object object, JSONObject jsonObject, boolean useSqlColumn) {
        if (jsonObject == null) {
            return null;
        }
        Object newObject = object;
        if (newObject == null) {
            try {
                newObject = cls.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }
        HashMap<String, JsonKeyHolder> holders = getHolders(cls);
        Iterator<String> keyIterator = jsonObject.keys();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            JsonKeyHolder holder = holders.get(key);
            if (holder != null) {
                if (!jsonObject.isNull(key)) {
                    try {
                        Field field = holder.classField.getField();
                        field.setAccessible(true);
                        if (SqlUtils.isLong(field)) {
                            field.setLong(newObject, jsonObject.optLong(holder.jsonName));
                        } else if (SqlUtils.isInteger(field)) {
                            field.setInt(newObject, jsonObject.optInt(holder.jsonName));
                        } else if (SqlUtils.isText(field)) {
                            field.set(newObject, jsonObject.optString(holder.jsonName));
                        } else if (SqlUtils.isBoolean(field)) {
                            field.set(newObject, jsonObject.optBoolean(holder.jsonName));
                        } else if (!SqlUtils.isArray(field) && !SqlUtils.isCollection(field)) {
                            field.set(newObject, fromJson(field.getType(), jsonObject.optJSONObject(holder.jsonName)));
                        } else {
                            Class<?> arrayObjectClass = null;
                            if (SqlUtils.isArray(field)) {
                                arrayObjectClass = field.getType().getComponentType();
                            } else if (SqlUtils.isCollection(field)) {
                                ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                                arrayObjectClass = (Class<?>) genericType.getActualTypeArguments()[0];
                            }

                            final JSONArray array = jsonObject.optJSONArray(key);
                            if (array != null && array.length() > 0) {
                                try {
                                    if (SqlUtils.isArray(field)) {
                                        final int size = array.length();
                                        final Object[] outObjects = (Object[]) Array.newInstance(arrayObjectClass, size);
                                        for (int j = 0; j < size; j++) {
                                            outObjects[j] = fromJson(arrayObjectClass, array.optJSONObject(j));
                                        }
                                        field.set(newObject, outObjects);
                                    } else if (SqlUtils.isCollection(field)) {
                                        final int size = array.length();
                                        Collection outCollection = null;
                                        outCollection = (Collection) field.getType().newInstance();
                                        for (int j = 0; j < size; j++) {
                                            outCollection.add(fromJson(arrayObjectClass, array.optJSONObject(j)));
                                        }
                                        field.set(newObject, outCollection);

                                    }
                                } catch (InstantiationException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return (T) newObject;
    }

    public static String toJson(Object object) {
        JsonBuilder builder = new JsonBuilder();
        toJsonBuilder(object, builder, false);
        return builder.build();
    }

    public static JsonBuilder toJsonBuilder(Object object) {
        JsonBuilder builder = new JsonBuilder();
        toJsonBuilder(object, builder, false);
        return builder;
    }

    public static void toJsonBuilder(Object object, JsonBuilder builder) {
        toJsonBuilder(object, builder, false);
    }

    public static void toJsonBuilder(Object object, JsonBuilder builder, boolean includeNull) {
        Class<?> selfClass = object.getClass();
        HashMap<String, JsonKeyHolder> holders = getHolders(selfClass);
        for (JsonKeyHolder keyHolder : holders.values()) {
            try {
                Field field = keyHolder.classField.getField();
                field.setAccessible(true);
                if (SqlUtils.isArray(field) || SqlUtils.isCollection(field)) {
                    Log.e(TAG, "Array and collections are not supported yet :(");
                    continue;
                }
                Object fieldValue = field.get(object);
                if (fieldValue == null) {
                    if (includeNull) {
                        if (SqlUtils.isText(field)) {
                            builder.Value(keyHolder.jsonName, JsonBuilder.EMPTY);
                        } else if (SqlUtils.isBoolean(field)) {
                            builder.Value(keyHolder.jsonName, false);
                        } else {
                            builder.Value(keyHolder.jsonName, 0);
                        }
                    }
                    continue;
                }
                builder.Value(keyHolder.jsonName, fieldValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static String resolveJsonKeyName(JsonKey key, Field field) {
        if (TextUtils.isEmpty(key.name())) {
            return field.getName();
        }
        return key.name();
    }

    private static String resolveJsonKeyName(JsonKey key, ClassField field) {
        if (TextUtils.isEmpty(key.name())) {
            return field.getFieldName();
        }
        return key.name();
    }

}
