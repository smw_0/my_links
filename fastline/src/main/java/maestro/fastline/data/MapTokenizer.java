package maestro.fastline.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by maestro123 on 08.07.2016.
 */
public class MapTokenizer implements Tokenizer {

    public static MapTokenizer create() {
        return new MapTokenizer(new HashMap<String, Object>());
    }

    public static MapTokenizer create(Map<String, Object> map) {
        return new MapTokenizer(map);
    }

    Map<String, Object> map;

    public MapTokenizer(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public void prepare(Class<?> selfClass) {
    }

    @Override
    public String resolveColumnName(String key) {
        return key;
    }

    @Override
    public Iterator<String> keyIterator() {
        return map.keySet().iterator();
    }

    @Override
    public Object value(String key) {
        return map.get(key);
    }

    @Override
    public int getCount() {
        return map.size();
    }

    @Override
    public boolean isNull(String key) {
        return map.get(key) == null;
    }

    public MapTokenizer add(String key, Object value) {
        map.put(key, value);
        return this;
    }

}