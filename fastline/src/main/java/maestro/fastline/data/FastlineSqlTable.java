package maestro.fastline.data;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by maestro123 on 6/15/2016.
 */
public class FastlineSqlTable {

    public static final String TEXT = "TEXT";
    public static final String REAL = "REAL";
    public static final String INTEGER = "INTEGER";
    public static final String DATETIME = "DATETIME";

    public static final String UNIQUE = "UNIQUE";
    public static final String NOT_NULL = "NOT NULL";
    public static final String PRIMARY_KEY = "PRIMARY KEY";
    public static final String AUTOINCREMENT = "AUTOINCREMENT";
    public static final String CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP";
    public static final String ON_DELETE_CASCADE = "ON DELETE CASCADE";

    public static final String SPACE = " ";
    public static final String COMMA = ",";
    public static final String BRACKET_LEFT = "(";
    public static final String BRACKET_RIGHT = ")";
    public static final String QUESTION = "?";
    public static final String SEMICOLON = ";";

    private final Map<String, String> mColumns = new LinkedHashMap<>();
    private final ArrayList<String> mForeign = new ArrayList<>();
    private final HashMap<String, Integer> mIndexMap = new HashMap<>();
    private final HashMap<String, String> mTriggers = new HashMap<>();

    private String mName;
    private Class<?> mClass;

    private String mCreate;
    private String mInsert;

    private boolean ifNotExists = true;
    private boolean isReplaceOnConflict = false;
    private boolean isFailOnConflict = false;
    private boolean isIgnoreOnConflict = true;

    public FastlineSqlTable(String name, Class<?> selfClass) {
        mName = name;
        mClass = selfClass;
    }

    public String getName() {
        return mName;
    }

    public Class<?> getSelfClass() {
        return mClass;
    }

    public void setIfNotExists(boolean ifNotExists) {
        this.ifNotExists = ifNotExists;
    }

    public void setReplaceOnConflict(boolean replaceOnConflict) {
        this.isReplaceOnConflict = replaceOnConflict;
    }

    public void put(String key, String... params) {
        StringBuilder builder = new StringBuilder();
        for (String param : params) {
            if (param != null) {
                builder.append(param).append(SPACE);
            }
        }
        mColumns.put(key, builder.toString());
    }

    public void putForeign(String column, String foreignTable, String foreignColumn, boolean onDeleteCascade) {
        mForeign.add(String.format("FOREIGN KEY (%s) REFERENCES %s (%s)", column, foreignTable, foreignColumn) + (onDeleteCascade ? " " + ON_DELETE_CASCADE : ""));
    }

    public void putTrigger(String name, String sql) {
        mTriggers.put(name, sql);
    }

    public HashMap<String, String> getTriggers() {
        return mTriggers;
    }

    public void remove(String key) {
        mColumns.remove(key);
    }

    public String create() {
        if (mCreate == null) {
            StringBuilder builder = new StringBuilder();
            builder.append("create table");
            if (ifNotExists) {
                builder.append(SPACE).append("if not exists");
            }
            builder.append(SPACE);
            builder.append(mName);
            builder.append(BRACKET_LEFT);

            boolean notFirst = false;

            for (String key : mColumns.keySet()) {
                if (notFirst) {
                    builder.append(COMMA);
                } else {
                    notFirst = true;
                }
                builder.append(key)
                        .append(SPACE)
                        .append(mColumns.get(key));
            }

            if (notFirst) {
                builder.append(SPACE);
            }

            for (String foreign : mForeign) {
                if (notFirst) {
                    builder.append(COMMA);
                } else {
                    notFirst = true;
                }
                builder.append(foreign)
                        .append(SPACE);
            }

            builder.append(BRACKET_RIGHT);
            builder.append(SEMICOLON);
            mCreate = builder.toString();
        }
        return mCreate;
    }

    public String insert() {
        if (mInsert == null) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT");
            if (isReplaceOnConflict) {
                builder.append(SPACE).append("OR REPLACE");
            } else if (isIgnoreOnConflict) {
                builder.append(SPACE).append("OR IGNORE");
            }
            builder.append(SPACE).append("INTO");
            builder.append(SPACE).append(mName);
            builder.append(BRACKET_LEFT);

            boolean notFirst = false;

            for (String key : mColumns.keySet()) {
                if (notFirst) {
                    builder.append(COMMA);
                } else {
                    notFirst = true;
                }
                builder.append(key);
            }
            builder.append(BRACKET_RIGHT);

            builder.append(SPACE).append("VALUES").append(SPACE);

            builder.append(BRACKET_LEFT).append(TextUtils.join(COMMA, Collections.nCopies(mColumns.size(), QUESTION)));

            builder.append(BRACKET_RIGHT).append(SEMICOLON);
            mInsert = builder.toString();
        }
        return mInsert;
    }

    public String insert(Map<String, String> objects) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT");
        if (isReplaceOnConflict) {
            builder.append(SPACE).append("OR REPLACE");
        } else if (isIgnoreOnConflict) {
            builder.append(SPACE).append("OR IGNORE");
        }
        builder.append(SPACE).append("INTO");
        builder.append(SPACE).append(mName).append(SPACE);

        boolean notFirst = false;

        StringBuilder keyBuilder = new StringBuilder();
        StringBuilder valuesBuilder = new StringBuilder();

        for (String key : objects.keySet()) {
            if (notFirst) {
                keyBuilder.append(COMMA);
                valuesBuilder.append(COMMA);
            } else {
                keyBuilder.append(BRACKET_LEFT);
                valuesBuilder.append(BRACKET_LEFT);
                notFirst = true;
            }
            keyBuilder.append(key);
            valuesBuilder.append(objects.get(key));
        }

        keyBuilder.append(BRACKET_RIGHT);
        valuesBuilder.append(BRACKET_RIGHT);

        builder.append(keyBuilder).append(SPACE).append("VALUES").append(SPACE).append(valuesBuilder);

        builder.append(SEMICOLON);
        return builder.toString();
    }

    public String update(String where, String... columns) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE");
        builder.append(SPACE).append(mName);
        builder.append(SPACE).append("SET").append(SPACE);

        boolean notFirst = false;
        int copiesSize = 0;

        if (columns == null || columns.length == 0) {
            copiesSize = mColumns.size();
            for (String key : mColumns.keySet()) {
                if (notFirst) {
                    builder.append(COMMA);
                } else {
                    notFirst = true;
                }
                builder.append(key).append("=?");
            }
        } else {
            copiesSize = columns.length;
            for (String key : columns) {
                if (notFirst) {
                    builder.append(COMMA);
                } else {
                    notFirst = true;
                }
                builder.append(key).append("=?");
            }
        }
        builder.append(where).append(SEMICOLON);
        return builder.toString();
    }

    public String update(String where, Map<String, String> objects) {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE");
        builder.append(SPACE).append(mName);
        builder.append(SPACE).append("SET").append(SPACE);

        boolean notFirst = false;

        for (String key : objects.keySet()) {
            if (notFirst) {
                builder.append(",");
            } else {
                notFirst = true;
            }
            builder.append(key).append("=").append(objects.get(key));
        }

        builder.append(SPACE).append(where).append(SEMICOLON);
        return builder.toString();
    }

    public int columnIndex(String column) {
        Integer cachedIndex = mIndexMap.get(column);
        if (cachedIndex == null) {
            int index = 0;
            cachedIndex = -1;
            for (String key : mColumns.keySet()) {
                if (column.equals(key)) {
                    cachedIndex = index;
                    mIndexMap.put(column, cachedIndex);
                    break;
                }
                index++;
            }
        }
        return cachedIndex;
    }

    public int bindIndex(String column) {
        return columnIndex(column) + 1;
    }

    public boolean containsColumn(String column) {
        return mColumns.containsKey(column);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[@" + Integer.toHexString(hashCode()) + "]: " + mName;
    }

    public int columnsCount() {
        return mColumns.size();
    }
}