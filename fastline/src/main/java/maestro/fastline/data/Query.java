package maestro.fastline.data;

/**
 * Created by maestro123 on 07.07.2016.
 */
public class Query {

    public static final String WHERE = "WHERE";
    public static final String SPACE = " ";
    public static final String AND = "AND";
    public static final String OR = "OR";
    public static final String IN = "IN";
    public static final String NOT = "NOT";
    public static final String LIMIT = "LIMIT";
    public static final String ORDER_BY = "ORDER BY";

    public static final String BRACKET_LEFT = "(";
    public static final String BRACKET_RIGHT = ")";
    public static final String COMMA = ",";

    private final StringBuilder builder = new StringBuilder();

    private boolean isClauseOpen;
    private boolean isFirstInClauseParams;
    private boolean isClauseParamsOpen;

    public static Query Create() {
        return new Query();
    }

    public static Query CreateWhere(){
        return new Query().Where();
    }

    public Query Where() {
        closeClauseIfOpen();
        builder.append(WHERE).append(SPACE).append(BRACKET_LEFT);
        isClauseOpen = true;
        isFirstInClauseParams = true;
        return this;
    }

    public Query In(String column, Object... values) {
        compileIn(column, false, values);
        return this;
    }

    public Query NotIn(String column, Object... values) {
        compileIn(column, true, values);
        return this;
    }

    public Query ColumnTrue(String column) {
        return ColumnEquals(column, Boolean.TRUE.toString());
    }

    public Query ColumnFalse(String column) {
        return ColumnEquals(column, Boolean.FALSE.toString());
    }

    public Query ColumnEquals(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append("=").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnNotEquals(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append("!=").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query NotNull(String column){
        onNewClauseParam();
        builder.append(column).append(SPACE).append("NOT NULL");
        return this;
    }

    public Query ColumnMore(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append(">").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnLess(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append("<").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnMoreOrEqual(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append(">=").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnLessOrEqual(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append("<=").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnLike(String column, Object value) {
        onNewClauseParam();
        builder.append(column).append(" LIKE ").append(SqlUtils.toSqlStatementValue(value));
        return this;
    }

    public Query ColumnLikeContains(String column, Object value) {
        return ColumnLikeContains(column, value, true);
    }

    public Query ColumnLikeContains(String column, Object value, boolean caseSensitive) {
        onNewClauseParam();
        builder.append(column).append(SPACE).append("LIKE").append(SPACE);
        if (!caseSensitive) {
            builder.append("LOWER(").append(SPACE);
        }
        builder.append(SqlUtils.toSqlStatementValue("%" + value + "%"));
        if (!caseSensitive) {
            builder.append(")");
        }
        return this;
    }

    public Query Or() {
        closeClauseParamIfOpen();
        builder.append(OR).append(SPACE);
        return this;
    }

    public Query And() {
        closeClauseParamIfOpen();
        builder.append(AND).append(SPACE);
        return this;
    }

    public Query Limit(int count) {
        closeClauseIfOpen();
        builder.append(LIMIT).append(SPACE).append(count);
        return this;
    }

    public Query OrderBy(String column, boolean asc) {
        closeClauseIfOpen();
        builder.append(ORDER_BY).append(SPACE).append(column)
                .append(SPACE).append(asc ? "ASC" : "DESC").append(SPACE);
        return this;
    }

    public String build() {
        closeClauseIfOpen();
        return builder.toString();
    }

    private void compileIn(String column, boolean not, Object... values) {
        closeClauseParamIfOpen();
        builder.append(column).append(SPACE);
        if (not) {
            builder.append(NOT).append(SPACE);
        }
        builder.append(IN).append(SPACE).append(BRACKET_LEFT);
        boolean isFirst = true;
        for (Object value : values) {
            if (!isFirst) {
                builder.append(COMMA);
            } else {
                isFirst = false;
            }
            builder.append(SqlUtils.toSqlStatementValue(value));
        }
        builder.append(BRACKET_RIGHT);
    }

    private void onNewClauseParam() {
        if (!isClauseParamsOpen) {
            builder.append(BRACKET_LEFT);
            isClauseParamsOpen = true;
        }
        if (!isFirstInClauseParams) {
            builder.append(COMMA);
            builder.append(SPACE);
        } else {
            isFirstInClauseParams = false;
        }
    }

    private void closeClauseParamIfOpen() {
        if (isClauseParamsOpen) {
            builder.append(BRACKET_RIGHT).append(SPACE);
            isClauseParamsOpen = false;
            isFirstInClauseParams = true;
        }
    }

    private void closeClauseIfOpen() {
        closeClauseParamIfOpen();
        if (isClauseOpen) {
            builder.append(BRACKET_RIGHT).append(SPACE);
            isClauseOpen = false;
        }
    }

}
