package maestro.fastline.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by maestro123 on 08.07.2016.
 */
public class JSONTokenizer implements Tokenizer {

    public static JSONTokenizer create(String jsonObject) {
        try {
            return new JSONTokenizer(new JSONObject(jsonObject));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONTokenizer create(JSONObject jsonObject) {
        return new JSONTokenizer(jsonObject);
    }

    public static JSONTokenizer[] list(JSONArray jsonArray){
        final int size = jsonArray.length();
        final JSONTokenizer[] tokenizers = new JSONTokenizer[size];
        for (int i = 0; i < size; i++) {
            tokenizers[i] = new JSONTokenizer(jsonArray.optJSONObject(i));
        }
        return tokenizers;
    }

    public static JSONTokenizer[] list(Class<?> cls, JSONObject... jsons){
        final int size = jsons.length;
        final JSONTokenizer[] tokenizers = new JSONTokenizer[size];
        for (int i = 0; i < size; i++) {
            tokenizers[i] = new JSONTokenizer(jsons[i]);
        }
        return tokenizers;
    }

    JSONObject jsonObject;
    HashMap<String, FastlineJson.JsonKeyHolder> holders;

    public JSONTokenizer(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public void prepare(Class<?> selfClass) {
        holders = FastlineJson.getHolders(selfClass);
    }

    @Override
    public int getCount() {
        return jsonObject.length();
    }

    @Override
    public Iterator<String> keyIterator() {
        return jsonObject.keys();
    }

    @Override
    public Object value(String key) {
        return jsonObject.opt(key);
    }

    @Override
    public boolean isNull(String key) {
        return jsonObject.isNull(key);
    }

    @Override
    public String resolveColumnName(String key) {
        return holders.containsKey(key) ? holders.get(key).getSqlColumnName() : null;
    }
}
