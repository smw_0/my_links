package maestro.fastline.data;

/**
 * Created by maestro123 on 7/10/2016.
 */
public class DoubleKeyArray<Key, AltKey, T> {

    private static final int INCREMENT_CAPACITY = 12;

    private Object[] mKeys = new Object[INCREMENT_CAPACITY];
    private Object[] mAlternateKeys = new Object[INCREMENT_CAPACITY];
    private Object[] mObjects = new Object[INCREMENT_CAPACITY];

    private int mSize = 0;

    public void put(Key key, AltKey altKey, T object) {
        checkCapacity();

        mKeys[mSize] = key;
        mAlternateKeys[mSize] = altKey;
        mObjects[mSize] = object;
        mSize += 1;
    }

    public T get(Object key) {
        if (key == null) {
            return null;
        }
        T value = getByPrimary(key);
        if (value == null) {
            value = getBySecondary(key);
        }
        return value;
    }

    public T getByPrimary(Object key) {
        return findByKey(mKeys, key);
    }

    public T getBySecondary(Object key) {
        return findByKey(mAlternateKeys, key);
    }

    private T findByKey(Object[] keys, Object key) {
        for (int i = 0; i < mSize; i++) {
            if (keys[i].equals(key)) {
                return (T) mObjects[i];
            }
        }
        return null;
    }

    private void checkCapacity() {
        if (mKeys.length == mSize) {
            final int newSize = mSize + INCREMENT_CAPACITY;
            mKeys = incrementArray(mKeys, newSize);
            mAlternateKeys = incrementArray(mAlternateKeys, newSize);
            mObjects = incrementArray(mObjects, newSize);
        }
    }

    private Object[] incrementArray(Object[] array, int newSize) {
        Object[] newArray = new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, array.length);
        return newArray;
    }

}
