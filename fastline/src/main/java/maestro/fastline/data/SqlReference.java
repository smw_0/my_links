package maestro.fastline.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by maestro123 on 6/29/2016.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SqlReference {
    Class<?> table() default Empty.class;

    String column() default "";

    String referenceColumn() default "";

    boolean cascadeDelete() default true;

    boolean cascadeDeleteWhenNoReference() default true;

    boolean constraintFromPrimary() default false;

    class Empty {
    }

}
