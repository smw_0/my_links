package maestro.fastline.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by maestro123 on 7/5/2016.
 */
public class FastlineSql extends SQLiteOpenHelper {

    public static final String TAG = FastlineSql.class.getSimpleName();

    private static volatile FastlineSql instance;

    public static synchronized FastlineSql CreateInstance(Context context, String name, int version) {
        return CreateInstance(context, name, version, true);
    }

    public static synchronized FastlineSql CreateInstance(Context context, String name, int version, boolean verifyConstraints) {
        if (instance == null) {
            synchronized (FastlineSql.class) {
                if (instance == null) {
                    instance = new FastlineSql(context, name, version, verifyConstraints);
                }
            }
        }
        return instance;
    }

    public static synchronized FastlineSql get() {
        return instance;
    }

    public static final String CONSTRAINT_TABLE_NAME = "constraint_%s_%s"; //first table name, second table name
    public static final String CONSTRAINT_COLUMN_NAME = "constraint_%s_%s"; //table name, origin column name

    public final HashMap<Class<?>, String> mTableNames = new HashMap<>();
    public final HashMap<Class<?>, FastlineSqlTable> mTables = new HashMap<>();
    public final HashMap<ClassPair, FastlineSqlTable> mConstraintTables = new HashMap<>();
    public final HashMap<Class<?>, DoubleKeyArray<ClassField, String, ColumnHolder>> mColumnHolders = new HashMap<>(); //use deifferemt keys!!!!!! Resolve this!
    public final HashMap<Class<?>, ColumnHolder> mPrimaryColumns = new HashMap<>();
    public final HashMap<Class<?>, ArrayList<ConstraintHolder>> mConstraintHolders = new HashMap<>();
    public final ArrayList<FastlineSqlTrigger> mTriggers = new ArrayList<>();

    private boolean isVerifyConstraints = true;

    //1 - trigger name, 2 - constraint table name, 3 - when clauses, 4 - refClass table name, 5 - delete clause
    private static final String CONSTRAINT_TRIGGER_TEMPLATE =
            "CREATE TRIGGER IF NOT EXISTS %s" + FastlineSqlTable.SPACE
                    + "AFTER" + FastlineSqlTable.SPACE
                    + "DELETE ON %s" + FastlineSqlTable.SPACE
                    + "WHEN (%s)" + FastlineSqlTable.SPACE
                    + "BEGIN" + FastlineSqlTable.SPACE
                    + "DELETE FROM %s WHERE (%s);" + FastlineSqlTable.SPACE
                    + "END";

    //1 - constraint table name, 2 - constrain column for track (refClassColumn), 3 = 2
    private static final String TRIGGER_WHEN_CLAUSE = "(SELECT COUNT(*) FROM %s WHERE %s = old.%s)=0";

    //1 - refClass column name, 2 - refClass constraint column name
    private static final String TRIGGER_DELETE_CLAUSE = "%s = old.%s";

    //1 - table name
    private static final String COUNT_OF_TEMPLATE = "SELECT COUNT(*) FROM %s";

    //1 - table name, 2 - where clauses
    private static final String COUNT_OF_WHERE_TEMPLATE = COUNT_OF_TEMPLATE + " %s";

    //1 - table name, 2 - column name, 3 - array of "?" that equals count of selection args
    private static final String CONSTRAINT_OBJECT_QUERY_TEMPLATE = "SELECT * FROM %s WHERE %s IN(%s)";

    //1 - select columns, 2 - table name, 3 - clause column, 4 - clause variable
    private static final String LINK_REFERENCE_QUERY_TEMPLATE = "SELECT %s FROM %s WHERE (%s=%s)";

    // 1 - constraint table name, 2 - reference table name, 3 - constraint table reference class column name,
    // 4 - reference table reference class column name, 5 - clause column, 6 - clause variable
    private static final String LINK_REFERENCE_QUERY_TEMPLATE_JOIN = "SELECT * FROM %s constrTable INNER JOIN %s refTable ON constrTable.%s=refTable.%s WHERE (%s=%s)";

    //1 - table name
    private static final String LIST_QUERY_TEMPLATE = "SELECT * FROM %s";

    //1 - table name, 2 - where clauses
    private static final String LIST_QUERY_WHERE_TEMPLATE = "SELECT * FROM %s %s";

    //1 - table name
    private static final String DELETE_QUERY_TEMPLATE = "DELETE FROM %s";

    //1 - table name, 2 - where clauses
    private static final String DELETE_QUERY_WHERE_TEMPLATE = "DELETE FROM %s %s";

    //1 - table name
    public static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS %s";

    //1 - trigger name
    public static final String DROP_TRIGGER_IF_EXISTS = "DROP TRIGGER IF EXISTS %s";

    private final HashMap<String, ArrayList<OnTableUpdateListener>> mListeners = new HashMap<>();

    public interface OnTableUpdateListener<T> {
        void onInsert(FastlineSqlTable table, List<T> items);

        void onUpdate(FastlineSqlTable table, List<T> items);

        void onDelete(FastlineSqlTable table, List<T> items);

        void onUnsatisfiedInsert(FastlineSqlTable table, Object... objectKeys);

        void onUnsatisfiedUpdate(FastlineSqlTable table, Object... objectKeys);

        void onUnsatisfiedDelete(FastlineSqlTable table, Object... objectKeys);
    }

    public FastlineSql(Context context, String name, int version) {
        this(context, name, version, true);
    }

    public FastlineSql(Context context, String name, int version, boolean verifyConstraints) {
        super(context, name, null, version);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (FastlineSqlTable table : mTables.values()) {
            Log.e(TAG, "table: " + table.create());
            db.execSQL(table.create());
        }
        for (FastlineSqlTable table : mConstraintTables.values()) {
            Log.e(TAG, "constraint table: " + table.create());
            db.execSQL(table.create());
        }
        for (FastlineSqlTrigger trigger : mTriggers) {
            Log.e(TAG, "trigger: " + trigger.getSql());
            db.execSQL(trigger.getSql());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (FastlineSqlTable table : mTables.values()) {
            db.execSQL(String.format(DROP_TABLE_IF_EXISTS, table.getName()));
        }
        for (FastlineSqlTable table : mConstraintTables.values()) {
            db.execSQL(String.format(DROP_TABLE_IF_EXISTS, table.getName()));
        }
        for (FastlineSqlTrigger trigger : mTriggers) {
            db.execSQL(String.format(DROP_TRIGGER_IF_EXISTS, trigger.getName()));
        }
        onCreate(db);
    }

    public void clearAllTables() {
        for (String tableName : mTableNames.values()) {
            getWritableDatabase().execSQL(String.format(DELETE_QUERY_TEMPLATE, tableName));
        }
    }

    public void begin() {
        getWritableDatabase().beginTransaction();
    }

    public void finish() {
        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }

    public void addListener(Class<?> trackClass, OnTableUpdateListener listener) {
        addListener(mTableNames.get(trackClass), listener);
    }

    public void removeListener(Class<?> trackClass, OnTableUpdateListener listener) {
        removeListener(mTableNames.get(trackClass), listener);
    }

    public void addListener(String tableName, OnTableUpdateListener listener) {
        synchronized (mListeners) {
            ArrayList<OnTableUpdateListener> listeners = mListeners.get(tableName);
            if (listeners == null) {
                listeners = new ArrayList<>();
                mListeners.put(tableName, listeners);
            }
            listeners.add(listener);
        }
    }

    public void removeListener(String tableName, OnTableUpdateListener listener) {
        synchronized (mListeners) {
            ArrayList<OnTableUpdateListener> listeners = mListeners.get(tableName);
            if (listeners == null) {
                return;
            }
            listeners.remove(listener);
        }
    }

    public enum ChangeType {
        Insert, Update, Delete
    }

    public void notifyTableChange(FastlineSqlTable table, ChangeType changeType, Object... objects) {
        notifyTableChange(table, changeType, Arrays.asList(objects), false);
    }

    public void notifyTableChange(FastlineSqlTable table, ChangeType changeType, boolean unsatisfied, Object... objects) {
        notifyTableChange(table, changeType, Arrays.asList(objects), unsatisfied);
    }

    public void notifyTableChange(FastlineSqlTable table, ChangeType changeType, List objects, boolean unsatisfied) {
        if (SqlUtils.isNullOrEmpty(objects)) {
            return;
        }
        synchronized (mListeners) {
            ArrayList<OnTableUpdateListener> listeners = mListeners.get(table.getName());
            if (listeners == null) {
                return;
            }
            //TODO: post on UI?
            for (OnTableUpdateListener listener : listeners) {
                switch (changeType) {
                    case Insert:
                        if (unsatisfied) {
                            listener.onUnsatisfiedInsert(table, changeType, objects);
                        } else {
                            listener.onInsert(table, objects);
                        }
                        break;
                    case Update:
                        if (unsatisfied) {
                            listener.onUnsatisfiedUpdate(table, changeType, objects);
                        } else {
                            listener.onUpdate(table, objects);
                        }
                        break;
                    case Delete:
                        if (unsatisfied) {
                            listener.onUnsatisfiedDelete(table, objects);
                        } else {
                            listener.onDelete(table, objects);
                        }
                        break;
                }
            }
        }
    }

    public void loadTables(Class<?>... classes) {
        for (Class<?> cls : classes) {
            FastlineSqlTable table = mTables.get(cls);
            if (table == null) {
                table = loadTable(cls);
            }
        }

        for (Class<?> cls : mConstraintHolders.keySet()) {

            ArrayList<ConstraintHolder> constraintHolders = mConstraintHolders.get(cls);
            if (constraintHolders == null || constraintHolders.size() == 0) {
                continue;
            }

            ArrayList<String> whenClause = new ArrayList<>();
            for (ConstraintHolder holder : constraintHolders) {
                whenClause.add(String.format(TRIGGER_WHEN_CLAUSE, holder.constraintTable.getName(), holder.refClassConstraintColumn, holder.refClassConstraintColumn));
            }

            final StringBuilder whenBuilder = new StringBuilder();

            for (String where : whenClause) {
                if (whenBuilder.length() > 0) {
                    whenBuilder
                            .append(FastlineSqlTable.SPACE)
                            .append("AND")
                            .append(FastlineSqlTable.SPACE);
                }
                whenBuilder.append(where);
            }

            for (ConstraintHolder holder : constraintHolders) {
                String triggerName = "cleanUpTrigger_" + holder.constraintTable.getName();
                String deleteClause = String.format(TRIGGER_DELETE_CLAUSE, holder.refClassColumn, holder.refClassConstraintColumn);
                String sqlTrigger = String.format(CONSTRAINT_TRIGGER_TEMPLATE,
                        triggerName,
                        holder.constraintTable.getName(),
                        whenBuilder.toString(),
                        holder.refClassTable.getName(),
                        deleteClause);

                FastlineSqlTrigger trigger = new FastlineSqlTrigger();
                trigger.name = triggerName;
                trigger.sql = sqlTrigger;

                mTriggers.add(trigger);
            }
        }
    }

    public FastlineSqlTable loadTable(Class<?> selfClass) {
        final String tableName = resolveTableName(selfClass);
        Log.e(TAG, "loadTable: " + tableName);

        FastlineSqlTable table = mTables.get(selfClass);
        if (table != null) {
            return table;
        }
        table = new FastlineSqlTable(tableName, selfClass);

        final ArrayList<String> sqlColumnParams = new ArrayList<>();
        final DoubleKeyArray<ClassField, String, ColumnHolder> columnHolders = new DoubleKeyArray<>();
        final ClassField[] fields = getSortedFields(selfClass);
        Arrays.sort(fields, FIELD_COMPARATOR);

        mTables.put(selfClass, table);
        mColumnHolders.put(selfClass, columnHolders);

        for (ClassField classField : fields) {
            Field field = classField.getField();
            SqlColumn column = field.getAnnotation(SqlColumn.class);
            if (column != null) {
                sqlColumnParams.clear();
                final String columnName = resolveColumnName(column, field);
                final String sqlType = toSqlType(column, field);

                final ColumnHolder holder = new ColumnHolder(
                        tableName,
                        columnName,
                        sqlType,
                        classField,
                        column.isDate(),
                        column.primary(),
                        column.autoincrement());

                columnHolders.put(classField, columnName, holder);

                if (sqlType != null) {
                    sqlColumnParams.add(sqlType);
                    if (column.unique()) {
                        sqlColumnParams.add(FastlineSqlTable.UNIQUE);
                    }
                    if (column.primary()) {
                        sqlColumnParams.add(FastlineSqlTable.PRIMARY_KEY);
                        mPrimaryColumns.put(selfClass, holder);
                    }
                    if (column.notNull()) {
                        sqlColumnParams.add(FastlineSqlTable.NOT_NULL);
                    }
                    if (column.autoincrement()) {
                        sqlColumnParams.add(FastlineSqlTable.AUTOINCREMENT);
                    }
                    if (column.isDate()) {
                        sqlColumnParams.add(FastlineSqlTable.CURRENT_TIMESTAMP);
                    }
                    if (haveForeign(column)) {
                        final Class<?> foreignClass = column.foreign();
                        final FastlineSqlTable foreignTable = loadTable(foreignClass);
                        final ColumnHolder foreignColumnHolder = findColumnOrPrimary(foreignClass, column.foreignColumn());

                        holder.foreignClass = foreignClass;
                        table.putForeign(columnName, foreignTable.getName(), foreignColumnHolder.name, column.deleteCascade());
                    }
                    table.put(columnName, sqlColumnParams.toArray(new String[sqlColumnParams.size()]));
                } else {
                    SqlReference sqlReference = field.getAnnotation(SqlReference.class);
                    if (sqlReference != null) {
                        Class<?> refClass = null;
                        if (SqlUtils.isArray(field)) {
                            refClass = field.getType().getComponentType();
                        } else if (SqlUtils.isCollection(field)) {
                            final ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                            refClass = (Class<?>) genericType.getActualTypeArguments()[0];
                        } else {
                            refClass = field.getType();
                        }
                        final FastlineSqlTable refClassTable = loadTable(refClass);
                        final String refTableName = refClassTable.getName();
                        //here is going "one to one", "one to many" and "many to many" connection

                        final String constraintTableName = resolveConstraintTableName(tableName, refTableName);
                        final FastlineSqlTable constraintTable = new FastlineSqlTable(constraintTableName, null);

                        mConstraintTables.put(ClassPair.create(selfClass, refClass), constraintTable);

                        final ColumnHolder selfRefColumn = findColumnOrPrimary(selfClass, sqlReference.column());
                        final ColumnHolder refClassRefColumn = findColumnOrPrimary(refClass, sqlReference.referenceColumn());

                        final ConstraintHolder constraintHolder = new ConstraintHolder();
                        constraintHolder.selfClassType = selfClass;
                        constraintHolder.refClassType = refClass;
                        constraintHolder.selfTable = table;
                        constraintHolder.refClassTable = refClassTable;
                        constraintHolder.constraintTable = constraintTable;
                        constraintHolder.selfColumnHolder = selfRefColumn;
                        constraintHolder.refClassColumnHolder = refClassRefColumn;
                        constraintHolder.selfColumn = selfRefColumn.name;
                        constraintHolder.refClassColumn = refClassRefColumn.name;
                        constraintHolder.constraintFromPrimary = sqlReference.constraintFromPrimary();

                        /*Insert here required for self to self constraint*/

                        ArrayList<ConstraintHolder> mTablesForTrig = mConstraintHolders.get(refClass);
                        if (mTablesForTrig == null) {
                            mTablesForTrig = new ArrayList<>();
                            mConstraintHolders.put(refClass, mTablesForTrig);
                        }
                        mTablesForTrig.add(constraintHolder);
                        holder.constraintHolder = constraintHolder;

                        final String selfConstraintColumnName = putConstraintColumn(constraintTable, selfRefColumn, false);
                        final String refClassConstraintColumnName = putConstraintColumn(constraintTable, refClassRefColumn, true);

                        constraintHolder.selfConstraintColumn = selfConstraintColumnName;
                        constraintHolder.refClassConstraintColumn = refClassConstraintColumnName;
                    }
                }
            }
        }
        return table;
    }

    private String resolveTableName(Class<?> cls) {
        String name = mTableNames.get(cls);
        if (name == null) {
            SqlTable table = cls.getAnnotation(SqlTable.class);
            if (table != null) {
                name = table.name();
                if (TextUtils.isEmpty(name)) {
                    name = cls.getSimpleName();
                }
                mTableNames.put(cls, name);
            }
        }
        return name;
    }

    private String resolveConstraintTableName(Class<?> selfClass, Class<?> refClass) {
        return String.format(CONSTRAINT_TABLE_NAME, resolveTableName(selfClass), resolveTableName(refClass));
    }

    private String resolveConstraintTableName(String selfTableName, String refTableName) {
        return String.format(CONSTRAINT_TABLE_NAME, selfTableName, refTableName);
    }

    private String resolveConstrainColumnName(ColumnHolder holder) {
        return String.format(CONSTRAINT_COLUMN_NAME, holder.tableName, holder.name);
    }

    private ColumnHolder findPrimaryColumn(Class<?> cls) {
        return mPrimaryColumns.get(cls);
    }

    private ColumnHolder findColumnOrPrimary(Class<?> cls, String columnName) {
        if (TextUtils.isEmpty(columnName)) {
            return findPrimaryColumn(cls);
        } else {
            return mColumnHolders.get(cls).get(columnName);
        }
    }

    private String putConstraintColumn(FastlineSqlTable table, ColumnHolder holder, boolean unique) {
        final String constraintColumnName = resolveConstrainColumnName(holder);
        table.put(constraintColumnName, holder.sqlType, unique ? FastlineSqlTable.UNIQUE : null);
        table.putForeign(constraintColumnName, holder.tableName, holder.name, true);
        return constraintColumnName;
    }

    private static String toSqlType(SqlColumn column, Field field) {
        if (column.isDate()) {
            return FastlineSqlTable.DATETIME;
        }else if (SqlUtils.isBoolean(field)){
            return FastlineSqlTable.INTEGER;
        } else if (SqlUtils.isText(field) || SqlUtils.isEnum(field)) {
            return FastlineSqlTable.TEXT;
        } else if (SqlUtils.isInteger(field) || SqlUtils.isLong(field)) {
            return FastlineSqlTable.INTEGER;
        } else if (SqlUtils.isDouble(field) || SqlUtils.isFloat(field)) {
            return FastlineSqlTable.REAL;
        }
        return null;
    }

    private static final Comparator<ClassField> FIELD_COMPARATOR = new Comparator<ClassField>() {
        @Override
        public int compare(ClassField lhs, ClassField rhs) {
            final boolean lhsSimple = isSimpleField(lhs);
            final boolean rhsSimple = isSimpleField(rhs);
            return lhsSimple && !rhsSimple ? -1 : !lhsSimple && rhsSimple ? 1 : 0;
        }

        private boolean isSimpleField(ClassField classField) {
            Field field = classField.getField();
            return SqlUtils.isInteger(field) || SqlUtils.isLong(field)
                    || SqlUtils.isText(field) || SqlUtils.isBoolean(field)
                    || SqlUtils.isFloat(field) || SqlUtils.isDouble(field)
                    || SqlUtils.isEnum(field);
        }

    };

    private HashMap<Class<?>, ClassField[]> mSortedFields = new HashMap<>();

    private ClassField[] getSortedFields(Class<?> cls) {
        ClassField[] fields = mSortedFields.get(cls);
        if (fields == null) {
            final Field[] declaredFields = cls.getDeclaredFields();
            final int size = declaredFields.length;
            fields = new ClassField[size];
            for (int i = 0; i < size; i++) {
                fields[i] = new ClassField(cls, null, declaredFields[i].getName());
            }
            Class<?> superClass = cls.getSuperclass();
            while (superClass != null && superClass != Object.class) {
                Field[] superFields = superClass.getDeclaredFields();
                if (!SqlUtils.isNullOrEmpty(superFields)) {
                    final int originSize = fields.length;
                    final int newSize = superFields.length;
                    fields = Arrays.copyOf(fields, originSize + newSize);
                    for (int i = 0; i < newSize; i++) {
                        fields[i + originSize] = new ClassField(cls, superClass, superFields[i].getName());
                    }
                }
                superClass = superClass.getSuperclass();
            }
            Arrays.sort(fields, FIELD_COMPARATOR);
            mSortedFields.put(cls, fields);
        }
        return fields;
    }

    /* insert and update*/

    public Object[] bulkInsert(Object... objects) {
        return bulkInsert(null, objects);
    }

    public Object[] bulkInsert(List list) {
        if (SqlUtils.isNullOrEmpty(list)) {
            return null;
        }
        try {
            begin();
            final int size = list.size();
            Object[] primaryKeyValues = new Object[size];
            for (int i = 0; i < size; i++) {
                FastlineTableStatement statement = insert(list.get(i), null, true);
                primaryKeyValues[i] = statement.PrimaryKeyValue;
            }
            return primaryKeyValues;
        } finally {
            finish();
        }
    }

    public Object[] bulkInsert(Map<String, Object> additionalParam, Object... objects) {
        if (SqlUtils.isNullOrEmpty(objects)) {
            return null;
        }
        try {
            begin();
            final int size = objects.length;
            Object[] primaryKeyValues = new Object[size];
            for (int i = 0; i < size; i++) {
                FastlineTableStatement statement = insert(objects[i], additionalParam, true);
                primaryKeyValues[i] = statement.PrimaryKeyValue;
            }
            return primaryKeyValues;
        } finally {
            finish();
        }
    }

    public Object[] bulkInsertWithConstraint(Class<?> constraintClass, Object constraintValue, Object... objects) {
        if (SqlUtils.isNullOrEmpty(objects)) {
            return null;
        }
        try {
            begin();
            final int size = objects.length;
            Object[] primaryKeyValues = new Object[size];
            for (int i = 0; i < size; i++) {
                FastlineTableStatement statement = insertWithConstraint(objects[i], constraintClass, constraintValue);
                primaryKeyValues[i] = statement.PrimaryKeyValue;
            }
            return primaryKeyValues;
        } finally {
            finish();
        }
    }

    public FastlineTableStatement insert(Object object) {
        return insert(object, null);
    }

    public FastlineTableStatement insert(Object object, Map<String, Object> additionalParams) {
        return insert(object, additionalParams, false);
    }

    public FastlineTableStatement insertOrUpdate(Object object) {
        return insertOrUpdate(object, null);
    }

    public FastlineTableStatement insertOrUpdate(Object object, Map<String, Object> additionalParams) {
        return insert(object, additionalParams, true);
    }

    public FastlineTableStatement insertWithConstraint(Object object, Class<?> constraintClass, Object constraintValue) {
        FastlineTableStatement statement = insert(object);
        if (statement.PrimaryKeyValue != null) {
            executeConstraint(constraintClass, statement.table.getSelfClass(), constraintValue, statement.PrimaryKeyValue);
        }
        return statement;
    }

    public FastlineTableStatement insert(Object object, Map<String, Object> additionalParams, boolean isUpdateAllow) {
        begin();
        try {
            final Class<?> selfClass = object.getClass();
            final FastlineTableStatement selfStatement = new FastlineTableStatement(mTables.get(selfClass));
            final DoubleKeyArray<ClassField, String, ColumnHolder> columnHolders = mColumnHolders.get(selfClass);
            final ClassField[] fields = getSortedFields(selfClass);

            boolean isInserted = false;

            for (ClassField classField : fields) {
                final ColumnHolder holder = columnHolders.getByPrimary(classField);
                if (holder == null) {
                    continue;
                }
                Field field = classField.getField();
                if (holder.getSqlType() != null) {
                    try {
                        field.setAccessible(true);
                        Object fieldValue = field.get(object);
                        if (holder.isPrimary) {
                            selfStatement.PrimaryColumnName = holder.name;
                            selfStatement.PrimaryKeyValue = fieldValue;
                        }
                        if (holder.isAutoincrement) {
                            selfStatement.isAutoincrementStatement = true;
                            if (!SqlUtils.isPrimitiveNull(field, fieldValue)) {
                                selfStatement.put(holder.name, fieldValue);
                            } else {
                                selfStatement.PrimaryKeyValue = null;
                            }
                        } else {
                            selfStatement.put(holder.name, fieldValue);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {

                    bindAdditionalParams(selfStatement, additionalParams);
                    executeInsertOrUpdate(selfStatement, object, isUpdateAllow, true);
                    isInserted = true;

                    //In theory we should reach this point when we store object it self

                    ConstraintHolder constraintHolder = holder.constraintHolder;
                    if (constraintHolder != null) {
                        try {
                            ArrayList<Object> linkedReferences = getLinkedReferences(selfStatement.PrimaryKeyValue, constraintHolder);

                            if (SqlUtils.isArray(field)) {
                                field.setAccessible(true);
                                Object[] objects = (Object[]) field.get(object);
                                if (objects != null) {
                                    for (Object refObject : objects) {
                                        Object keyObject = processRefClassLink(constraintHolder, selfStatement, refObject, true);
                                        linkedReferences.remove(keyObject);
                                    }
                                }
                            } else if (SqlUtils.isCollection(field)) {
                                field.setAccessible(true);
                                Collection<Object> objects = (Collection<Object>) field.get(object);
                                if (objects != null) {
                                    for (Object refObject : objects) {
                                        Object keyObject = processRefClassLink(constraintHolder, selfStatement, refObject, true);
                                        linkedReferences.remove(keyObject);
                                    }
                                }
                            } else {
                                field.setAccessible(true);
                                Object refObject = field.get(object);
                                if (refObject == null) {
                                    if (constraintHolder.constraintFromPrimary) {
                                        if (processSelfConstraintLink(constraintHolder, selfStatement)) {
                                            linkedReferences.remove(selfStatement.PrimaryKeyValue);
                                        }
                                    }
                                } else {
                                    //!=null???
                                    if (constraintHolder.refClassColumnHolder.foreignClass != null
                                            && constraintHolder.refClassColumnHolder.foreignClass.isAssignableFrom(selfClass)) {
                                        Field selfForeignLinkField = constraintHolder.selfColumnHolder.classField.getField();
                                        Field refClassForeignLinkField = constraintHolder.refClassColumnHolder.classField.getField();
                                        selfForeignLinkField.setAccessible(true);
                                        refClassForeignLinkField.setAccessible(true);
                                        refClassForeignLinkField.set(refObject, selfForeignLinkField.get(object));
                                    }

                                    Object keyObject = processRefClassLink(constraintHolder, selfStatement, refObject, true);
                                    linkedReferences.remove(keyObject);
                                }
                            }

                            if (linkedReferences.size() > 0 && !isUpdateAllow) {
                                StringBuilder cleanupQuery = new StringBuilder("DELETE FROM " + constraintHolder.constraintTable.getName()
                                        + " WHERE " + constraintHolder.refClassConstraintColumn
                                        + " IN (");
                                boolean isFirst = true;
                                for (Object linkedKeyObject : linkedReferences) {
                                    if (!isFirst) {
                                        cleanupQuery.append(FastlineSqlTable.COMMA);
                                    } else {
                                        isFirst = false;
                                    }
                                    cleanupQuery.append(SqlUtils.toSqlStatementValue(linkedKeyObject));
                                }
                                cleanupQuery.append(")");
                                lockExecute(getWritableDatabase().compileStatement(cleanupQuery.toString()));
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (!isInserted) {
                bindAdditionalParams(selfStatement, additionalParams);
                executeInsertOrUpdate(selfStatement, object, true, true);
                isInserted = true;
            }
            if (selfStatement.isAutoincrementStatement && selfStatement.PrimaryKeyValue != null) {
                ColumnHolder primaryColumn = findPrimaryColumn(selfClass);
                try {
                    Field field = selfClass.getDeclaredField(primaryColumn.getFieldName());
                    field.setAccessible(true);
                    field.set(object, selfStatement.PrimaryKeyValue);
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return selfStatement;
        } finally {
            finish();
        }
    }

    private void lockExecute(SQLiteStatement statement) {
        try {
            statement.execute();
        } finally {
            statement.close();
        }
    }

    private long lockExecuteInsert(SQLiteStatement statement) {
        try {
            return statement.executeInsert();
        } finally {
            statement.close();
        }
    }

    private int lockExecuteUpdate(SQLiteStatement statement) {
        try {
            return statement.executeUpdateDelete();
        } finally {
            statement.close();
        }
    }

    private void bindAdditionalParams(FastlineTableStatement selfStatement, Map<String, Object> additionalParams) {
        if (additionalParams != null && additionalParams.size() > 0) {
            for (String key : additionalParams.keySet()) {
                selfStatement.put(key, additionalParams.get(key));
            }
        }
    }

    //TODO: check if reference exists???
    private boolean processSelfConstraintLink(ConstraintHolder constraintHolder, FastlineTableStatement statement) {
        return processConstraintLink(constraintHolder, statement.PrimaryKeyValue, statement.PrimaryKeyValue);
    }

    //TODO: check if reference exists???
    private boolean processConstraintLink(ConstraintHolder constraintHolder, Object selfValue, Object refClassValue) {

        FastlineTableStatement refLinkStatement = new FastlineTableStatement(constraintHolder.constraintTable);
        refLinkStatement.put(constraintHolder.selfConstraintColumn, selfValue);
        refLinkStatement.put(constraintHolder.refClassConstraintColumn, refClassValue);

        return executeInsertOrUpdate(refLinkStatement, null, false, false); //TODO: enable update exists statements?
    }

    //TODO: check if reference exists???
    private Object processRefClassLink(ConstraintHolder constraintHolder, FastlineTableStatement selfStatement, Object refObject, boolean isUpdateAllow) {
        FastlineTableStatement refInsertStatement = insert(refObject, null, isUpdateAllow);

        processConstraintLink(constraintHolder, selfStatement.PrimaryKeyValue, refInsertStatement.PrimaryKeyValue);

        return refInsertStatement.PrimaryKeyValue;
    }

    private boolean executeInsertOrUpdate(FastlineTableStatement statement, Object object, boolean isUpdateAllow, boolean isNotifyAllow) {
        long id = lockExecuteInsert(getWritableDatabase().compileStatement(statement.compileInsert()));
        if (id == -1 && isUpdateAllow) {
            int updateCount = lockExecuteUpdate(getWritableDatabase().compileStatement(statement.compileUpdate()));
            if (updateCount > 0) {
                if (isNotifyAllow) {
                    notifyTableChange(statement.table, ChangeType.Update, object); // <- how to pass object?
                }
                return true;
            }
        } else {
            if (isNotifyAllow && id != -1) {
                notifyTableChange(statement.table, ChangeType.Insert, object);
            }
            if (statement.isAutoincrementStatement) {
                statement.PrimaryKeyValue = (int) id; //SHOULD BE ALWAYS STRING OR INTEGER???
            }
        }
        return id != -1;
    }

    private void executeConstraint(Class<?> selfClass, Class<?> refClass, Object selfConstraintValue, Object refConstraintValue) {
        ConstraintHolder constraintHolder = findConstraintHolder(selfClass, refClass);
        if (constraintHolder != null) {
            insertConstraintSelf(constraintHolder, selfConstraintValue);
            processConstraintLink(constraintHolder, selfConstraintValue, refConstraintValue);
        }
    }

    /**
     * Additional constraint logic
     */

    public void bulkConstraintInsert(Class<?> selfClass, Class<?> refClass, Object... values) {
        try {
            begin();
            final ConstraintHolder constraintHolder = findConstraintHolder(selfClass, refClass);
            if (constraintHolder != null) {
                for (Object value : values) {
                    FastlineTableStatement selfStatement = insertConstraintSelf(constraintHolder, value);
                    processSelfConstraintLink(constraintHolder, selfStatement);
                }
            }
        } finally {
            finish();
        }
    }

    public FastlineTableStatement insertConstraintSelf(ConstraintHolder constraintHolder, Object value) {
        final FastlineTableStatement selfStatement = new FastlineTableStatement(constraintHolder.selfTable);
        selfStatement.put(constraintHolder.selfColumn, value);
        selfStatement.PrimaryColumnName = constraintHolder.selfColumn;
        selfStatement.PrimaryKeyValue = value;
        executeInsertOrUpdate(selfStatement, null, true, true);
        return selfStatement;
    }

    private Cursor getLinkedReferencesCursor(Object primaryKeyValue, ConstraintHolder constraintHolder) {
        return getReadableDatabase().rawQuery(String.format(LINK_REFERENCE_QUERY_TEMPLATE_JOIN,
                constraintHolder.constraintTable.getName(), //1 - constraint table name,
                constraintHolder.refClassTable.getName(), //2 - reference table name,
                constraintHolder.refClassConstraintColumn, //3 - constraint table reference class column name,
                constraintHolder.refClassColumn, //4 - reference table reference class column name,
                constraintHolder.selfConstraintColumn,
                SqlUtils.toSqlStatementValue(primaryKeyValue)), null);
    }

    private ArrayList<Object> getLinkedReferences(Object primaryKeyValue, ConstraintHolder constraintHolder) {
        ArrayList<Object> linkedReferences = new ArrayList<>();
        Cursor linkedReferenceCursor = null;
        try {
            linkedReferenceCursor = getReadableDatabase().rawQuery(String.format(LINK_REFERENCE_QUERY_TEMPLATE,
                    constraintHolder.refClassConstraintColumn,
                    constraintHolder.constraintTable.getName(),
                    constraintHolder.selfConstraintColumn,
                    SqlUtils.toSqlStatementValue(primaryKeyValue)), null);
            if (linkedReferenceCursor != null && linkedReferenceCursor.getCount() > 0) {
                while (linkedReferenceCursor.moveToNext()) {
                    int columnType = linkedReferenceCursor.getType(0);
                    if (columnType == Cursor.FIELD_TYPE_STRING) {
                        linkedReferences.add(linkedReferenceCursor.getString(0));
                    } else if (columnType == Cursor.FIELD_TYPE_INTEGER) {
                        linkedReferences.add(linkedReferenceCursor.getInt(0));
                    }
                }
            }
        } finally {
            if (linkedReferenceCursor != null) {
                linkedReferenceCursor.close();
            }
        }
        return linkedReferences;
    }

    /* Maps and JSON*/

    /*
    * Key will taken from insert value (use it if classes primary keys are foreign)
    * */

    public void insertWithConstraintBuild(Class<?> selfClass, Class<?> refClass, Object constraintValue) {
        ConstraintHolder constraintHolder = findConstraintHolder(selfClass, refClass);
        if (constraintHolder != null) {
            insertConstraintSelf(constraintHolder, constraintValue);
            processConstraintLink(constraintHolder, constraintValue, constraintValue);
        }
    }

    public FastlineTableStatement insertWithConstraintBuild(Class<?> selfClass, Class<?> constraintClass, Tokenizer tokenizer) {
        FastlineTableStatement selfStatement = tokenizeInsert(selfClass, tokenizer);
        if (selfStatement.PrimaryKeyValue != null) {
            executeConstraint(constraintClass, selfClass, selfStatement.PrimaryKeyValue, selfStatement.PrimaryKeyValue);
        }
        return selfStatement;
    }

    public FastlineTableStatement insertWithConstraintBuild(Class<?> selfClass, Class<?> constraintClass, Object constraintObjectValue, Tokenizer tokenizer) {
        FastlineTableStatement selfStatement = tokenizeInsert(selfClass, tokenizer);
        if (selfStatement.PrimaryKeyValue != null) {
            executeConstraint(constraintClass, selfClass, constraintObjectValue, selfStatement.PrimaryKeyValue);
        }
        return selfStatement;
    }

    /*
    * Key will taken from insert values (use it if classes primary keys are foreign)
    * */

    public Object[] bulkConstraintInsert(Class<?> selfClass, Class<?> constraintClasses, Tokenizer... values) {
        return bulkConstraintInsert(selfClass, new Class<?>[]{constraintClasses}, values);
    }

    public Object[] bulkConstraintInsert(Class<?> selfClass, Class<?>[] constraintClasses, Tokenizer... values) {
        try {
            begin();
            Object[] primaryKeys = bulkTokenizeInsert(selfClass, values);
            for (Class<?> constraintClass : constraintClasses) {
                ConstraintHolder constraintHolder = findConstraintHolder(constraintClass, selfClass);
                if (constraintHolder != null) {
                    for (Object value : primaryKeys) {
                        insertConstraintSelf(constraintHolder, value);
                        processConstraintLink(constraintHolder, value, value);
                    }
                }
            }
            return primaryKeys;
        } finally {
            finish();
        }
    }

    public Object[] bulkConstraintInsert(Class<?> selfClass, Class<?> constraintClasses, Object constraintPrimaryValue, Tokenizer... values) {
        return bulkConstraintInsert(selfClass, new Class<?>[]{constraintClasses}, constraintPrimaryValue, values);
    }

    public Object[] bulkConstraintInsert(Class<?> selfClass, Class<?>[] constraintClasses, Object constraintPrimaryValue, Tokenizer... values) {
        try {
            begin();
            Object[] primaryKeys = bulkTokenizeInsert(selfClass, values);
            for (Class<?> constraintClass : constraintClasses) {
                ConstraintHolder constraintHolder = findConstraintHolder(constraintClass, selfClass);
                if (constraintHolder != null) {
                    insertConstraintSelf(constraintHolder, constraintPrimaryValue);
                    bulkConstraintLink(constraintHolder, constraintPrimaryValue, primaryKeys);
                }
            }
            return primaryKeys;
        } finally {
            finish();
        }
    }

    public void bulkConstraintLink(Class<?> selfClass, Class<?> refClass, Object constrainColumnValue, Object... values) {
        ConstraintHolder constraintHolder = findConstraintHolder(selfClass, refClass);
        if (constraintHolder != null) {
            bulkConstraintLink(constraintHolder, constrainColumnValue, values);
        }
    }

    public void bulkConstraintLink(ConstraintHolder constraintHolder, Object constrainColumnValue, Object... values) {
        try {
            begin();
            for (Object value : values) {
                processConstraintLink(constraintHolder, constrainColumnValue, value);
            }
        } finally {
            finish();
        }
    }

    /*
    * Tokenize inserts
    * */

    public Object[] bulkTokenizeInsert(Class<?> selfClass, Tokenizer... objects) {
        if (SqlUtils.isNullOrEmpty(objects)) {
            return null;
        }
        try {
            begin();
            final int size = objects.length;
            Object[] primaryKeyValues = new Object[size];
            for (int i = 0; i < size; i++) {
                FastlineTableStatement statement = tokenizeInsert(selfClass, objects[i]);
                primaryKeyValues[i] = statement.PrimaryKeyValue;
            }
            return primaryKeyValues;
        } finally {
            finish();
        }
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, Tokenizer tokenizer) {
        return tokenizeInsert(selfClass, null, null, null, tokenizer);
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, Object primaryValue, Tokenizer tokenizer) {
        return tokenizeInsert(selfClass, primaryValue, null, tokenizer);
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, Object primaryValue, Map<String, Object> additionalParams, Tokenizer tokenizer) {
        ColumnHolder primaryColumn = findPrimaryColumn(selfClass);
        if (primaryColumn != null) {
            return tokenizeInsert(selfClass, primaryColumn.name, primaryValue, additionalParams, tokenizer);
        }
        return null;
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, Map<String, Object> additionalParams, Tokenizer tokenizer) {
        return tokenizeInsert(selfClass, null, null, additionalParams, tokenizer);
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, String primaryColumn, Object primaryValue, Tokenizer tokenizer) {
        return tokenizeInsert(selfClass, primaryColumn, primaryValue, null, tokenizer);
    }

    public FastlineTableStatement tokenizeInsert(Class<?> selfClass, String primaryKey, Object primaryValue, Map<String, Object> additionalParams, Tokenizer tokenizer) {
        final FastlineTableStatement selfStatement = new FastlineTableStatement(mTables.get(selfClass));
        final DoubleKeyArray<ClassField, String, ColumnHolder> columnHolders = mColumnHolders.get(selfClass);

        tokenizer.prepare(selfClass);
        final Iterator<String> keyIterator = tokenizer.keyIterator();

        ArrayList<ConstraintHolder> mPendingConstraints = null;

        while (keyIterator.hasNext()) {
            final String key = keyIterator.next();
            if (tokenizer.isNull(key)) {
                continue;
            }
            final String columnName = tokenizer.resolveColumnName(key);

            ColumnHolder holder = columnHolders.getBySecondary(columnName);

            if (holder != null) {
                Object value = tokenizer.value(key);
                selfStatement.put(columnName, value);
                if (holder.isPrimary) {
                    selfStatement.PrimaryColumnName = holder.name;
                    selfStatement.PrimaryKeyValue = value;
                    if (holder.foreignClass != null) {
                        ConstraintHolder constraintHolder = findConstraintHolder(selfClass, holder.foreignClass);
                        if (constraintHolder != null) {
                            if (mPendingConstraints == null) {
                                mPendingConstraints = new ArrayList<>();
                            }
                            mPendingConstraints.add(constraintHolder);
                        }
                    }
                } else if (holder.constraintHolder != null) {
                    if (mPendingConstraints == null) {
                        mPendingConstraints = new ArrayList<>();
                    }
                    mPendingConstraints.add(holder.constraintHolder);
                }
            }
        }

        bindAdditionalParams(selfStatement, additionalParams);

        if (primaryKey != null && primaryValue != null) {
            selfStatement.PrimaryColumnName = primaryKey;
            selfStatement.PrimaryKeyValue = primaryValue;
        }

        executeInsertOrUpdate(selfStatement, null, true, true);
        if (mPendingConstraints != null) {
            for (ConstraintHolder constraintHolder : mPendingConstraints) {
                processSelfConstraintLink(constraintHolder, selfStatement);
            }
        }
        return selfStatement;
    }

    /**
     * Constraint helpers
     */

    private final HashMap<ClassPair, ConstraintHolder> mPairsConstraintHoldersCache = new HashMap<>();

    private ConstraintHolder findConstraintHolder(Class<?> selfClass, Class<?> refClass) {
        ClassPair classPair = ClassPair.create(selfClass, refClass);
        ConstraintHolder constraintHolder = mPairsConstraintHoldersCache.get(classPair);
        if (mPairsConstraintHoldersCache.containsKey(classPair)) {
            return mPairsConstraintHoldersCache.get(classPair);
        }
        ArrayList<ConstraintHolder> constraintHolders = mConstraintHolders.get(refClass);
        if (constraintHolders != null && constraintHolders.size() >= 0) {
            for (ConstraintHolder holder : constraintHolders) {
                if (selfClass.equals(holder.selfClassType)) {
                    constraintHolder = holder;
                    break;
                }
            }
        }
        mPairsConstraintHoldersCache.put(classPair, constraintHolder);
        return constraintHolder;
    }

    /**
     * List
     */

    public <T> T get(Class<?> selfClass, Object primaryValue) {
        ColumnHolder columnHolder = findPrimaryColumn(selfClass);
        ArrayList<T> list = list(selfClass, Query.Create().Where().ColumnEquals(columnHolder.name, primaryValue).Limit(1));
        return list.size() > 0 ? list.get(0) : null;
    }

    public <T> T get(Class<?> selfClass, Query query) {
        ColumnHolder columnHolder = findPrimaryColumn(selfClass);
        ArrayList<T> list = list(selfClass, query.Limit(1));
        return list.size() > 0 ? list.get(0) : null;
    }

    public <T> T getFirst(Class<?> selfClass) {
        return getFirst(selfClass, null);
    }

    public <T> T getFirst(Class<?> selfClass, Query query) {
        ArrayList<T> list = list(selfClass, query);
        if (!SqlUtils.isNullOrEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    public <T> T getLast(Class<?> selfClass) {
        return getLast(selfClass, null);
    }

    public <T> T getLast(Class<?> selfClass, Query query) {
        ArrayList<T> list = list(selfClass, query);
        if (!SqlUtils.isNullOrEmpty(list)) {
            return list.get(list.size() - 1);
        }
        return null;
    }

    public <T> ArrayList<T> list(Class<?> selfClass) {
        return listQuery(selfClass, null, null);
    }

    public <T> ArrayList<T> list(Class<?> selfClass, Query query) {
        return listQuery(selfClass, query != null ? query.build() : null, null);
    }

    public <T> ArrayList<T> listQuery(Class<?> selfClass, String where, String order) {
        final FastlineSqlTable table = mTables.get(selfClass);
        final ArrayList<T> outList = new ArrayList<>();
        Cursor selfCursor = null;
        try {
            String query = null;
            if (where != null) {
                query = String.format(LIST_QUERY_WHERE_TEMPLATE, table.getName(), where);
            } else {
                query = String.format(LIST_QUERY_TEMPLATE, table.getName());
            }

            if (order != null) {
                query += " " + order;
            }

            selfCursor = getReadableDatabase().rawQuery(query, null);

            if (selfClass != null && selfCursor.getCount() > 0) {
                try {
                    begin();
                    while (selfCursor.moveToNext()) {
                        try {
                            outList.add((T) createObject(selfClass, selfCursor));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                } finally {
                    finish();
                }
            }
        } finally {
            if (selfCursor != null) {
                selfCursor.close();
            }
        }
        return outList;
    }

    /*
    * Object create
    * */

    private Object createObject(Class<?> selfClass, Cursor cursor) throws IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        final FastlineSqlTable selfTable = mTables.get(selfClass);
        Constructor constructor = selfClass.getConstructor();
        constructor.setAccessible(true);
        final Object newObject = constructor.newInstance();
        final ClassField[] fields = getSortedFields(selfClass);
        final DoubleKeyArray<ClassField, String, ColumnHolder> columnHolders = mColumnHolders.get(selfClass);
        Object mSelfPrimaryKey = null;
        for (ClassField classField : fields) {
            ColumnHolder holder = columnHolders.getByPrimary(classField);
            if (holder != null) {
                Field field = classField.getField();
                if (holder.getSqlType() == null) {
                    ConstraintHolder constraintHolder = holder.constraintHolder;
                    if (constraintHolder != null) {
                        Cursor refClassCursor = null;
                        try {
                            refClassCursor = getLinkedReferencesCursor(mSelfPrimaryKey, constraintHolder);
                            if (refClassCursor != null && refClassCursor.getCount() > 0) {
                                field.setAccessible(true);
                                if (SqlUtils.isArray(field)) {
                                    final int size = refClassCursor.getCount();
                                    final Object[] outObjects = (Object[]) Array.newInstance(constraintHolder.refClassType, size);
                                    for (int j = 0; j < size; j++) {
                                        refClassCursor.moveToPosition(j);
                                        outObjects[j] = createObject(constraintHolder.refClassType, refClassCursor);
                                    }
                                    field.set(newObject, outObjects);
                                } else if (SqlUtils.isCollection(field)) {
                                    final int size = refClassCursor.getCount();
                                    Collection outCollection = (Collection) field.getType().newInstance();
                                    for (int j = 0; j < size; j++) {
                                        refClassCursor.moveToPosition(j);
                                        outCollection.add(createObject(constraintHolder.refClassType, refClassCursor));
                                    }
                                    field.set(newObject, outCollection);
                                } else {
                                    refClassCursor.moveToFirst();
                                    field.set(newObject, createObject(constraintHolder.refClassType, refClassCursor));
                                }
                            }
                        } finally {
                            if (refClassCursor != null) {
                                refClassCursor.close();
                            }
                        }
                    }
                } else {
                    final int columnIndex = cursor.getColumnIndexOrThrow(holder.name);//selfTable.columnIndex(holder.name);
                    if (cursor.isNull(columnIndex)) {
                        continue;
                    }

                    field.setAccessible(true);
                    if (SqlUtils.isLong(field)) {
                        field.setLong(newObject, cursor.getLong(columnIndex));
                    } else if (SqlUtils.isInteger(field)) {
                        field.setInt(newObject, cursor.getInt(columnIndex));
                    } else if (SqlUtils.isFloat(field)) {
                        field.setFloat(newObject, cursor.getFloat(columnIndex));
                    } else if (SqlUtils.isDouble(field)) {
                        field.setDouble(newObject, cursor.getDouble(columnIndex));
                    } else if (SqlUtils.isBoolean(field)) {
                        field.set(newObject, cursor.getInt(columnIndex) == 1);
                    } else if (field.getType().isEnum()) {
                        field.set(newObject, Enum.valueOf((Class<Enum>) field.getType(), cursor.getString(columnIndex)));
                    } else {
                        field.set(newObject, SqlUtils.backwardString(cursor.getString(columnIndex)));
                    }
                    if (holder.isPrimary) {
                        mSelfPrimaryKey = field.get(newObject);
                    }
                }
            }
        }
        return newObject;
    }

    /*
    * Delete
    * */

    private class DeleteStatement {

        private FastlineSqlTable table;
        private String columnName;
        private ArrayList<String> primaryValues = new ArrayList<>();
        private ArrayList<Object> affectObjects = new ArrayList<>();

        public DeleteStatement(FastlineSqlTable table, String columnName) {
            this.table = table;
            this.columnName = columnName;
        }

        public void addPrimaryValue(String value) {
            primaryValues.add(value);
        }

        public String compile() {
            StringBuilder builder = new StringBuilder()
                    .append("DELETE FROM ")
                    .append(table.getName())
                    .append(" WHERE ")
                    .append(columnName)
                    .append(" IN (");
            boolean isFirst = true;
            for (String value : primaryValues) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    builder.append(FastlineSqlTable.COMMA);
                }
                builder.append(value);
            }
            builder.append(")");
            return builder.toString();
        }

        public void addAffectedObject(Object object) {
            affectObjects.add(object);
        }

        public Object[] collectAffectedObjects() {
            return affectObjects.toArray();
        }

    }

    public int delete(Class<?> selfClass, Query query) {
        return delete(selfClass, query.build());
    }

    public int delete(Class<?> selfClass, String where) {
        FastlineSqlTable table = mTables.get(selfClass);
        SQLiteStatement statement = null;
        if (where != null) {
            statement = getWritableDatabase().compileStatement(String.format(DELETE_QUERY_WHERE_TEMPLATE, table.getName(), where));
        } else {
            statement = getWritableDatabase().compileStatement(String.format(DELETE_QUERY_TEMPLATE, table.getName()));
        }
        try {
            return statement.executeUpdateDelete();
        } finally {
            statement.close();
        }
    }

    public int delete(Object... objects) {
        if (SqlUtils.isNullOrEmpty(objects)) {
            return 0;
        }
//        Arrays.sort(objects); //Sort items for easy delete of multiple object type
        final HashMap<Class<?>, DeleteStatement> deleteStatements = new HashMap<>();

        Class<?> lastClass = null;
        FastlineSqlTable lastTable = null;
        ColumnHolder lastColumnHolder = null;
        for (Object object : objects) {
            Class<?> objectClass = object.getClass();
            if (!objectClass.equals(lastClass)) {
                lastClass = objectClass;
                lastTable = mTables.get(lastClass);
                lastColumnHolder = mPrimaryColumns.get(lastClass);
            }
            DeleteStatement statement = deleteStatements.get(lastClass);
            if (statement == null) {
                statement = new DeleteStatement(lastTable, lastColumnHolder.name);
                deleteStatements.put(lastClass, statement);
            }

            try {
                Field field = lastColumnHolder.classField.getField();
                field.setAccessible(true);
                statement.addPrimaryValue(SqlUtils.toSqlStatementValue(field.get(object)));
                statement.addAffectedObject(object);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        int deleteCount = 0;
        try {
            begin();
            for (DeleteStatement deleteStatement : deleteStatements.values()) {
                SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(deleteStatement.compile());
                try {
                    deleteCount = sqLiteStatement.executeUpdateDelete();
                } finally {
                    sqLiteStatement.close();
                }
                notifyTableChange(deleteStatement.table, ChangeType.Delete, deleteStatement.collectAffectedObjects());
            }
            return deleteCount;
        } finally {
            finish();
        }
    }

    public void clear(Class<?> selfClass) {
        String tableName = mTableNames.get(selfClass);
        if (!TextUtils.isEmpty(tableName)) {
            getWritableDatabase().execSQL(String.format(DELETE_QUERY_TEMPLATE, tableName));
        }
    }

    /*
    * Count
    * */

    public long countOf(Class<?> cls) {
        return getReadableDatabase()
                .compileStatement(String.format(COUNT_OF_TEMPLATE, mTables.get(cls).getName()))
                .simpleQueryForLong();
    }

    public long countOf(Class<?> cls, Query query) {
        return countOf(cls, query.build());
    }

    public long countOf(Class<?> cls, String where) {
        return getReadableDatabase()
                .compileStatement(String.format(COUNT_OF_WHERE_TEMPLATE, mTables.get(cls).getName(), where))
                .simpleQueryForLong();
    }

    public long countOf(String tableName, String where) {
        return getReadableDatabase()
                .compileStatement(
                        TextUtils.isEmpty(where)
                                ? String.format(COUNT_OF_TEMPLATE, tableName)
                                : String.format(COUNT_OF_WHERE_TEMPLATE, tableName, where))
                .simpleQueryForLong();
    }

    /*
    * Additional methods
    * */

    public static String resolveColumnName(SqlColumn column, Field field) {
        String name = column.name();
        if (TextUtils.isEmpty(name)) {
            return field.getName();
        }
        return name;
    }

    public static String resolveColumnName(SqlColumn column, ClassField field) {
        String name = column.name();
        if (TextUtils.isEmpty(name)) {
            return field.getFieldName();
        }
        return name;
    }

    private boolean haveForeign(SqlColumn sqlColumn) {
        return !SqlColumn.Empty.class.isAssignableFrom(sqlColumn.foreign());
    }

    /*
    * Additional classes
    * */


    public static class ConstraintHolder {

        FastlineSqlTable constraintTable;
        FastlineSqlTable selfTable;
        FastlineSqlTable refClassTable;

        ColumnHolder selfColumnHolder;
        ColumnHolder refClassColumnHolder;

        String selfColumn;
        String refClassColumn;

        String selfConstraintColumn;
        String refClassConstraintColumn;

        Class<?> selfClassType;
        Class<?> refClassType;

        boolean constraintFromPrimary;

    }

    public static class FastlineSqlTrigger {

        String name;
        String sql;

        public String getSql() {
            return sql;
        }

        public String getName() {
            return name;
        }
    }

    public static class FastlineTableStatement {

        private FastlineSqlTable table;

        private ArrayList<FastlineTableStatement> mAfterCompileStatements = new ArrayList<>();

        private Object PrimaryKeyValue;
        private String PrimaryColumnName;

        private boolean isAutoincrementStatement;

        public FastlineTableStatement(FastlineSqlTable table) {
            this.table = table;
        }

        public Map<String, String> map = new HashMap<>();

        public void put(String columnName, Object value) {
            map.put(columnName, SqlUtils.toSqlStatementValue(value));
        }

        public String compileInsert() {
            return table.insert(map);
        }

        public String compileUpdate(String where) {
            return table.update(where, map);
        }

        public String compileUpdate() {
            return table.update("WHERE " + PrimaryColumnName + "=" + SqlUtils.toSqlStatementValue(PrimaryKeyValue), map);
        }

        public void addAfterCompileStatement(FastlineTableStatement statement) {
            mAfterCompileStatements.add(statement);
        }
    }

    public static class ColumnHolder {

        String name;
        String sqlType;
        String tableName;

        ClassField classField;

        Class<?> foreignClass;
        ConstraintHolder constraintHolder;

        boolean isDate;
        boolean isPrimary;
        boolean isAutoincrement;

        public ColumnHolder(String tableName, String name, String sqlType, ClassField classField, boolean isDate, boolean isPrimary, boolean isAutoincrement) {
            this.name = name;
            this.isDate = isDate;
            this.sqlType = sqlType;
            this.tableName = tableName;
            this.isPrimary = isPrimary;
            this.classField = classField;
            this.isAutoincrement = isAutoincrement;
        }

        public String getName() {
            return name;
        }

        public String getSqlType() {
            return sqlType;
        }

        public String getFieldName() {
            return classField.getFieldName();
        }

    }

    public static class ClassPair {

        Class<?> first;
        Class<?> second;

        public static ClassPair create(Class<?> first, Class<?> second) {
            ClassPair pair = new ClassPair();
            pair.first = first;
            pair.second = second;
            return pair;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof ClassPair) {
                ClassPair oPair = (ClassPair) o;
                return first.equals(oPair.first) && second.equals(oPair.second);
            }
            return super.equals(o);
        }

        @Override
        public int hashCode() {
            return first.hashCode() + second.hashCode();
        }
    }

}
