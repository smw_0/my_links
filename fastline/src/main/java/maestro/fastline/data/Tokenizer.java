package maestro.fastline.data;

import java.util.Iterator;

/**
 * Created by maestro123 on 08.07.2016.
 */
public interface Tokenizer {

    void prepare(Class<?> selfClass);

    Iterator<String> keyIterator();

    String resolveColumnName(String key);

    Object value(String key);

    int getCount();

    boolean isNull(String key);

}