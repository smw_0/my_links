package maestro.fastline.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by maestro123 on 6/29/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SqlColumn {
    String name() default "";

    boolean primary() default false;

    boolean autoincrement() default false;

    boolean unique() default false;

    boolean notNull() default false;

    Class<?> foreign() default Empty.class;

    String foreignColumn() default "";

    boolean deleteCascade() default true;

    boolean isTokenizeKeyLinkable() default true;

    boolean isDate() default false;

    class Empty {
    }

}
