package maestro.fastline.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

/**
 * Created by maestro123 on 7/5/2016.
 */
public class FullBackgroundRelativeLayout extends RelativeLayout {

    public static final String TAG = FullBackgroundRelativeLayout.class.getSimpleName();

    private Drawable backgroundDrawable;

    public FullBackgroundRelativeLayout(Context context) {
        super(context);
        init();
    }

    public FullBackgroundRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FullBackgroundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        setWillNotDraw(false);
    }

    @Override
    public void setBackgroundDrawable(Drawable backgroundDrawable) {
        this.backgroundDrawable = backgroundDrawable;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (backgroundDrawable != null) {
            if (backgroundDrawable instanceof ColorDrawable) {
                backgroundDrawable.setBounds(0, 0, getScreenWidth(), getScreenHeight());
                backgroundDrawable.draw(canvas);
            } else {
                backgroundDrawable.setBounds(0, 0, getScreenWidth(), getScreenHeight());
                backgroundDrawable.draw(canvas);
            }
        } else {
            super.onDraw(canvas);
        }
    }

    private int getScreenWidth() {
        return getResources().getDisplayMetrics().widthPixels;
    }

    private int getScreenHeight() {
        return getResources().getDisplayMetrics().heightPixels;
    }

}
