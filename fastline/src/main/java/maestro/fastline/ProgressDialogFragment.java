package maestro.fastline;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by maestro123 on 11.07.2016.
 */
public class ProgressDialogFragment extends DialogFragment {

    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";

    private static final String ARG_TITLE_RESOURCE = "titleResource";
    private static final String ARG_MESSAGE_RESOURCE = "messageResource";

    public static ProgressDialogFragment create(int message) {
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        Bundle args = new Bundle(1);
        args.putInt(ARG_MESSAGE_RESOURCE, message);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProgressDialogFragment create(int title, int message) {
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        Bundle args = new Bundle();
        if (title != -1) {
            args.putInt(ARG_TITLE_RESOURCE, title);
        }
        if (message != -1) {
            args.putInt(ARG_MESSAGE_RESOURCE, message);
        }
        fragment.setArguments(args);
        return fragment;
    }

    public static ProgressDialogFragment create(String title, String message) {
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        Bundle args = new Bundle(2);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProgressDialogFragment show(FragmentManager manager, int message, String tag) {
        return showIfAllow(ProgressDialogFragment.create(message), manager, tag);
    }

    public static ProgressDialogFragment showIfAllow(ProgressDialogFragment fragment, FragmentManager manager, String tag) {
        Fragment existFragment = manager.findFragmentByTag(tag);
        if (existFragment == null) {
            fragment.show(manager, tag);
            return fragment;
        }
        return (ProgressDialogFragment) existFragment;
    }

    public static void dismiss(FragmentManager manager, String tag) {
        DialogFragment fragment = (DialogFragment) manager.findFragmentByTag(tag);
        if (fragment != null) {
            fragment.dismissAllowingStateLoss();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity(), getTheme());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setTitle(dialog);
        setMessage(dialog);
        dialog.setIndeterminate(true);
        return dialog;
    }

    private void setTitle(ProgressDialog dialog) {
        if (getArguments() != null) {
            if (getArguments().containsKey(ARG_TITLE)) {
                dialog.setTitle(getArguments().getString(ARG_TITLE));
            } else if (getArguments().containsKey(ARG_TITLE_RESOURCE)) {
                dialog.setTitle(getArguments().getInt(ARG_TITLE_RESOURCE));
            }
        }
    }

    private void setMessage(ProgressDialog dialog) {
        if (getArguments() != null) {
            if (getArguments().containsKey(ARG_MESSAGE)) {
                dialog.setMessage(getArguments().getString(ARG_MESSAGE));
            } else if (getArguments().containsKey(ARG_MESSAGE_RESOURCE)) {
                dialog.setMessage(getString(getArguments().getInt(ARG_MESSAGE_RESOURCE)));
            }
        }
    }

}