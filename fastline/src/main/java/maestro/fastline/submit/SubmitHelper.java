package maestro.fastline.submit;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maestro123 on 28.06.2016.
 */
public class SubmitHelper {

    public static final String TAG = SubmitHelper.class.getSimpleName();

    private static volatile SubmitHelper instance;

    public static synchronized SubmitHelper get() {
        if (instance == null) {
            synchronized (SubmitHelper.class) {
                if (instance == null) {
                    instance = new SubmitHelper();
                }
            }
        }
        return instance;
    }

    public static Intent PickIntent(String type) {
        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }
        intent.setType(type);
        return intent;
    }

    private final ArrayList<OnSubmitResultListener> mListeners = new ArrayList<>();
    private final Handler uiHandler = new Handler(Looper.getMainLooper());

    private Context mContext;
    private ExecutorService mSubmitExecutor;
    private SubmitAdditionalResolver mAdditionalResolver;

    public void configure(Context context, boolean singleThread) {
        mContext = context.getApplicationContext();
        mSubmitExecutor = singleThread ? Executors.newSingleThreadExecutor() : Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
    }

    public void submit(Uri uri) {
        if (uri == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                mContext.getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mSubmitExecutor.execute(new SubmitTask(uri));
    }

    public void submit(Uri[] uris) {
        for (Uri uri : uris) {
            submit(uri);
        }
    }

    public void submit(ArrayList<Uri> uris) {
        for (Uri uri : uris) {
            submit(uri);
        }
    }

    public void registerListener(OnSubmitResultListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void unregisterListener(OnSubmitResultListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    private void notifySubmitSuccess(SubmitTask task) {
        synchronized (mListeners) {
            for (OnSubmitResultListener listener : mListeners) {
                uiHandler.post(new SubmitSuccessNotifyTask(listener, task.data));
            }
        }
    }

    private void notifySubmitError(SubmitTask task) {
        synchronized (mListeners) {
            for (OnSubmitResultListener listener : mListeners) {
                uiHandler.post(new SubmitErrorNotifyTask(listener, task.uri));
            }
        }
    }

    public void setAdditionalResolver(SubmitAdditionalResolver resolver) {
        mAdditionalResolver = resolver;
    }

    public static void handleResult(Intent data) {
        if (data != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && data.getClipData() != null) {
                ClipData clipdata = data.getClipData();
                for (int i = 0; i < clipdata.getItemCount(); i++) {
                    get().submit(clipdata.getItemAt(i).getUri());
                }
            } else if (data.getData() != null) {
                get().submit(data.getData());
            }
        }
    }

    private abstract class SubmitNotifyTask<T> implements Runnable {

        private T data;
        private WeakReference<OnSubmitResultListener> listenerReference;

        public SubmitNotifyTask(OnSubmitResultListener listener, T data) {
            this.data = data;
            listenerReference = new WeakReference<OnSubmitResultListener>(listener);
        }

        @Override
        public void run() {
            OnSubmitResultListener listener = listenerReference.get();
            if (listener != null) {
                notify(listener, data);
            }
        }

        public abstract void notify(OnSubmitResultListener listener, T data);

    }

    private final class SubmitErrorNotifyTask extends SubmitNotifyTask<Uri> {

        public SubmitErrorNotifyTask(OnSubmitResultListener listener, Uri data) {
            super(listener, data);
        }

        @Override
        public void notify(OnSubmitResultListener listener, Uri data) {
            listener.onSubmitError(data);
        }

    }

    private final class SubmitSuccessNotifyTask extends SubmitNotifyTask<SubmitData> {

        public SubmitSuccessNotifyTask(OnSubmitResultListener listener, SubmitData data) {
            super(listener, data);
        }

        @Override
        public void notify(OnSubmitResultListener listener, SubmitData data) {
            listener.onSubmitSuccess(data);
        }
    }

    private class SubmitTask implements Runnable {

        private Uri uri;
        private SubmitData data;

        public SubmitTask(Uri uri) {
            this.uri = uri;
            data = new SubmitData();
            data.setUri(uri);
        }

        @Override
        public void run() {
            if ("content".equals(uri.getScheme())) {
                Cursor cursor = null;
                try {
                    try {
                        cursor = mContext.getContentResolver().query(uri,
                                new String[]{
                                        MediaStore.Files.FileColumns.DISPLAY_NAME,
                                        MediaStore.Files.FileColumns.SIZE
                                }, null, null, null);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                    if (cursor == null || cursor.getCount() == 0) {
                        notifySubmitError(this);
                        return;
                    }
                    cursor.moveToFirst();
                    data.setName(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME)));
                    data.setSize(cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.SIZE)));
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }

                if (TextUtils.isEmpty(data.getName())) {
                    notifySubmitError(this);
                    return;
                }

                if (data.getSize() == 0) {
                    InputStream is = null;
                    try {
                        is = mContext.getContentResolver().openInputStream(uri);
                        if (is == null) {
                            notifySubmitError(this);
                            return;
                        }
                        data.setSize(is.available());
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (data.getSize() == 0) {
                    File cacheFile = new File(mContext.getCacheDir(), ".utemp");
                    InputStream is = null;
                    OutputStream os = null;
                    try {
                        is = mContext.getContentResolver().openInputStream(uri);
                        if (is == null) {
                            notifySubmitError(this);
                            return;
                        }
                        os = new BufferedOutputStream(new FileOutputStream(cacheFile));
                        int bufferSize = 1024;
                        byte[] buffer = new byte[bufferSize];
                        int len = 0;
                        while ((len = is.read(buffer)) != -1) {
                            os.write(buffer, 0, len);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (os != null) {
                            try {
                                os.flush();
                                os.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    data.setSize(cacheFile.length());
                    if (data.getSize() == 0) {
                        notifySubmitError(this);
                        return;
                    }
                }
                if (mAdditionalResolver != null) {
                    mAdditionalResolver.resolve(data);
                }
                notifySubmitSuccess(this);
            }
        }
    }

    public interface OnSubmitResultListener {
        void onSubmitSuccess(SubmitData data);

        void onSubmitError(Uri uri);
    }

    public static abstract class SubmitAdditionalResolver {

        public abstract void resolve(SubmitData data);

    }

}
