package smw.mylinks.core.model;

import maestro.fastline.data.SqlColumn;
import maestro.fastline.data.SqlTable;
import smw.mylinks.core.Keys;

/**
 * Created by maestro123 on 8/7/16.
 */
@SqlTable
public class Link {

    @SqlColumn(name = Keys.HASH, primary = true, notNull = true, unique = true)
    private String mHash;

    @SqlColumn(name = Keys.LINK)
    private String mLink;

    @SqlColumn(name = Keys.TITLE)
    private String mTitle;

    @SqlColumn(name = Keys.IMAGE)
    private String mImage;

    @SqlColumn(name = Keys.DATE_CREATED)
    private long mDateCreated;

    public void setHash(String hash) {
        mHash = hash;
    }

    public String getHash() {
        return mHash;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getLink() {
        return mLink;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getImage() {
        return mImage;
    }

    public void setDateCreated(long dateCreated) {
        mDateCreated = dateCreated;
    }

    public long getDateCreated() {
        return mDateCreated;
    }
}
