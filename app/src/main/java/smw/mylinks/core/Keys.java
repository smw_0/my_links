package smw.mylinks.core;

/**
 * Created by maestro123 on 8/6/16.
 */
public class Keys {

    public static final String HASH = "hash";
    public static final String LINK = "link";
    public static final String TITLE = "title";
    public static final String IMAGE = "image";
    public static final String DATE_CREATED = "dateCreated";

}
