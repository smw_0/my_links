package smw.mylinks;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import maestro.fastline.FastlineUtils;
import maestro.fastline.recycle.TextDecoration;
import smw.mylinks.core.model.Link;

/**
 * Created by maestro123 on 8/6/16.
 */
public class LinksListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Link>>{

    public static final String TAG = LinksListFragment.class.getSimpleName();

    private static final int LOADER_ID = 0;

    private RecyclerView mList;
    private LinearLayoutManager mListManager;

    private LinksAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_links_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mList = (RecyclerView) view.findViewById(R.id.list);

        mListManager = new LinearLayoutManager(getContext());
        mAdapter = new LinksAdapter(getContext());

        final TextDecoration mDecoration = new TextDecoration(getContext(), R.style.ListHeaderStyle);
        mDecoration.setStyle(getContext(), 1, R.style.ListDecorationStyle);
        mDecoration.setHeaderStartOffset(FastlineUtils.dp(getContext(), 16));

        mList.addItemDecoration(mDecoration);
        mList.setLayoutManager(mListManager);
        mList.setAdapter(mAdapter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<List<Link>> onCreateLoader(int id, Bundle args) {
        return new LinksLoader(getContext());
    }

    @Override
    public void onLoadFinished(Loader<List<Link>> loader, List<Link> data) {
        mAdapter.update(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Link>> loader) {

    }

    public static class LinksLoader extends AsyncTaskLoader<List<Link>> {

        public LinksLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public List<Link> loadInBackground() {
            return null;
        }
    }
}
