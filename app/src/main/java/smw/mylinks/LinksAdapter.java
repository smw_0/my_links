package smw.mylinks;

import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import maestro.fastline.data.SqlUtils;
import maestro.fastline.recycle.TextDecoration;
import smw.mylinks.core.model.Link;

/**
 * Created by maestro123 on 8/7/16.
 */
public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.LinkViewHolder> implements TextDecoration.DecorationHeaderAdapter{

    private Context mContext;
    private SortedList<Link> mLinks = new SortedList<Link>(Link.class, new SortedList.Callback<Link>() {
        @Override
        public int compare(Link o1, Link o2) {
            return Long.compare(o2.getDateCreated(), o1.getDateCreated());
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(Link oldItem, Link newItem) {
            return oldItem.getLink().equals(newItem.getLink());
        }

        @Override
        public boolean areItemsTheSame(Link item1, Link item2) {
            return item1.getHash().equals(item2.getHash());
        }
    });

    public LinksAdapter(Context context) {
        mContext = context;

        Link link = new Link();
        link.setHash("Test");
        link.setTitle("Test title");
        link.setImage("http://www.freedigitalphotos.net/images/img/homepage/87357.jpg");
        link.setLink("https://www.google.by");

        Link link2 = new Link();
        link2.setHash("Second test");
        link2.setTitle("Second test title");
        link2.setImage("http://www.freedigitalphotos.net/images/previews/series-of-business-professionals-at-work-10038962.jpg");
        link2.setLink("eer");

        mLinks.add(link);
        mLinks.add(link2);

    }

    @Override
    public LinkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LinkViewHolder(View.inflate(mContext, R.layout.link_list_item_view, null));
    }

    @Override
    public void onBindViewHolder(LinkViewHolder holder, int position) {
        holder.bind(mLinks.get(position));
    }

    @Override
    public int getItemCount() {
        return mLinks.size();
    }

    public void update(List<Link> links) {
        if (!SqlUtils.isNullOrEmpty(links)) {
            mLinks.addAll(links);
        }
    }

    @Override
    public CharSequence getHeaderText(int position) {
        return "TestHeader";
    }

    @Override
    public boolean haveHeader(int position) {
        return position == 0;
    }

    public class LinkViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImage;

        private TextView txtTitle;
        private TextView txtSource;

        public LinkViewHolder(View itemView) {
            super(itemView);

            mImage = (ImageView) itemView.findViewById(R.id.image);

            txtTitle = (TextView) itemView.findViewById(R.id.title);
            txtSource = (TextView) itemView.findViewById(R.id.source);
        }

        public void bind(Link link) {
            Picasso.with(itemView.getContext())
                    .load(link.getImage())
                    .resizeDimen(R.dimen.image_width, R.dimen.image_height)
                    .centerCrop()
                    .into(mImage);

            txtTitle.setText(link.getTitle());
            txtSource.setText(link.getLink());
        }

    }

}
